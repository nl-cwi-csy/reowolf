use super::*;

#[test]
fn test_binary_literals() {
    Tester::new_single_source_expect_ok("valid", "
        func test() -> u32 {
            u8  v1 = 0b0100_0010;
            u16 v2 = 0b10101010;
            u32 v3 = 0b10000001_01111110;
            u64 v4 = 0b1001_0110_1001_0110;

            return 0b10110;
        }
    ");

    Tester::new_single_source_expect_err("invalid character", "
        func test() -> u32 {
            return 0b10011001_10012001;
        }
    ").error(|e| { e.assert_msg_has(0, "incorrectly formatted binary number"); });

    Tester::new_single_source_expect_err("no characters", "
        func test() -> u32 { return 0b; }
    ").error(|e| { e.assert_msg_has(0, "incorrectly formatted binary number"); });

    Tester::new_single_source_expect_err("only separators", "
        func test() -> u32 { return 0b____; }
    ").error(|e| { e.assert_msg_has(0, "incorrectly formatted binary number"); });
}

#[test]
fn test_string_literals() {
    Tester::new_single_source_expect_ok("valid", "
        func test() -> string {
            auto v1 = \"Hello, world!\";
            auto v2 = \"\\t\\r\\n\\\\\"; // why hello there, confusing thing
            auto v3 = \"\";
            return \"No way, dude!\";
        }
    ").for_function("test", |f| { f
        .for_variable("v1", |v| { v.assert_concrete_type("string"); })
        .for_variable("v2", |v| { v.assert_concrete_type("string"); })
        .for_variable("v3", |v| { v.assert_concrete_type("string"); });
    });

    Tester::new_single_source_expect_err("unterminated simple", "
        func test() -> string { return \"'; }
    ").error(|e| { e
        .assert_num(1)
        .assert_occurs_at(0, "\"")
        .assert_msg_has(0, "unterminated");
    });

    Tester::new_single_source_expect_err("unterminated with preceding escaped", "
        func test() -> string { return \"\\\"; }
    ").error(|e| { e
        .assert_num(1)
        .assert_occurs_at(0, "\"\\")
        .assert_msg_has(0, "unterminated");
    });

    Tester::new_single_source_expect_err("invalid escaped character", "
        func test() -> string { return \"\\y\"; }
    ").error(|e| { e.assert_msg_has(0, "unsupported escape character 'y'"); });

    // Note sure if this should always be in here...
    Tester::new_single_source_expect_err("non-ASCII string", "
        func test() -> string { return \"💧\"; }
    ").error(|e| { e.assert_msg_has(0, "non-ASCII character in string literal"); });
}

#[test]
fn test_bytestring_literals() {
    Tester::new_single_source_expect_ok("valid", "
        func test() -> u8[] {
            auto v1 = b\"Hello, world!\";
            auto v2 = b\"\\t\\r\\n\\\\\"; // why hello there, confusing thing
            auto v3 = b\"\";
            return b\"No way, dude!\";
        }
    ").for_function("test", |f| { f
        .for_variable("v1", |v| { v.assert_concrete_type("u8[]"); })
        .for_variable("v2", |v| { v.assert_concrete_type("u8[]"); })
        .for_variable("v3", |v| { v.assert_concrete_type("u8[]"); });
    });

    Tester::new_single_source_expect_err("unterminated simple", "
        func test() -> u8[] { return b\"'; }
    ").error(|e| { e
        .assert_num(1)
        .assert_occurs_at(0, "b\"")
        .assert_msg_has(0, "unterminated");
    });

    Tester::new_single_source_expect_err("unterminated with preceding escaped", "
        func test() -> u8[] { return b\"\\\"; }
    ").error(|e| { e
        .assert_num(1)
        .assert_occurs_at(0, "b\"\\")
        .assert_msg_has(0, "unterminated");
    });

    Tester::new_single_source_expect_err("invalid escaped character", "
        func test() -> u8[] { return b\"\\y\"; }
    ").error(|e| { e.assert_msg_has(0, "unsupported escape character 'y'"); });

    // Note sure if this should always be in here...
    Tester::new_single_source_expect_err("non-ASCII string", "
        func test() -> u8[] { return b\"💧\"; }
    ").error(|e| { e.assert_msg_has(0, "non-ASCII character in string literal"); });
}

#[test]
fn test_tuple_literals() {
    Tester::new_single_source_expect_ok("zero tuples", "
        func test() -> () {
            // Looks like lisp :)
            auto t1 = ();
            () t2 = ();
            auto t3 = (());
            () t4 = (());
            auto t5 = ((((()))));
            ((())) t6 = ((((()))));

            return ();
        }
    ").for_function("test", |f| { f
        .for_variable("t1", |v| { v.assert_concrete_type("()"); })
        .for_variable("t2", |v| { v.assert_concrete_type("()"); })
        .for_variable("t3", |v| { v.assert_concrete_type("()"); })
        .for_variable("t4", |v| { v.assert_concrete_type("()"); })
        .for_variable("t5", |v| { v.assert_concrete_type("()"); })
        .for_variable("t6", |v| { v.assert_concrete_type("()"); });
    });

    // All one-tuples (T) are transformed into T to prevent ambiguity
    Tester::new_single_source_expect_ok("one tuples", "
        func test() -> (u32) {
            auto a = (0);
            (s32) b = (1);
            ((((s32)))) c = ((2));
        }
    ").for_function("test", |f| { f
        .for_variable("a", |v| { v.assert_concrete_type("s32"); })
        .for_variable("b", |v| { v.assert_concrete_type("s32"); })
        .for_variable("c", |v| { v.assert_concrete_type("s32"); });
    });

    Tester::new_single_source_expect_ok("actual tuples", "
        func test() -> (u32, u32) {
            (u8,u16,u32) a = (0, 1, 2);
            auto b = a;
            auto c = (3, 4, 5);
            ((auto, auto)) d = (a, c);
            auto e = (\"hello\", 'c', 5 + 2);
            return ((0), (1));
        }
    ").for_function("test", |f| { f
        .for_variable("a", |v| { v.assert_concrete_type("(u8,u16,u32)"); })
        .for_variable("b", |v| { v.assert_concrete_type("(u8,u16,u32)"); })
        .for_variable("c", |v| { v.assert_concrete_type("(s32,s32,s32)"); })
        .for_variable("d", |v| { v.assert_concrete_type("((u8,u16,u32),(s32,s32,s32))"); })
        .for_variable("e", |v| { v.assert_concrete_type("(string,char,s32)"); });
    });
}