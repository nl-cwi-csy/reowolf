use super::*;

#[test]
fn test_simple_struct_definition() {
    Tester::new_single_source_expect_ok(
        "empty struct",
        "struct Foo{}"
    ).for_struct("Foo", |t| { t.assert_num_fields(0); });

    Tester::new_single_source_expect_ok(
        "single field, no comma",
        "struct Foo{ s32 field }"
    ).for_struct("Foo", |t| { t
        .assert_num_fields(1)
        .for_field("field", |f| {
            f.assert_parser_type("s32");
        });
    });

    Tester::new_single_source_expect_ok(
        "single field, with comma",
        "struct Foo{ s32 field, }"
    ).for_struct("Foo", |t| { t
        .assert_num_fields(1)
        .for_field("field", |f| { f
            .assert_parser_type("s32");
        });
    });

    Tester::new_single_source_expect_ok(
        "multiple fields, no comma",
        "struct Foo{ u8 a, s16 b, s32 c }"
    ).for_struct("Foo", |t| { t
        .assert_num_fields(3)
        .for_field("a", |f| { f.assert_parser_type("u8"); })
        .for_field("b", |f| { f.assert_parser_type("s16"); })
        .for_field("c", |f| { f.assert_parser_type("s32"); });
    });

    Tester::new_single_source_expect_ok(
        "multiple fields, with comma",
        "struct Foo{
            u8 a,
            s16 b,
            s32 c,
        }"
    ).for_struct("Foo", |t| { t
        .assert_num_fields(3)
        .for_field("a", |f| { f.assert_parser_type("u8"); })
        .for_field("b", |f| { f.assert_parser_type("s16"); })
        .for_field("c", |f| { f.assert_parser_type("s32"); });
    });
}