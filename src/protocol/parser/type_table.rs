/**
 * type_table.rs
 *
 * The type table is a lookup from AST definition (which contains just what the
 * programmer typed) to a type with additional information computed (e.g. the
 * byte size and offsets of struct members). The type table should be considered
 * the authoritative source of information on types by the compiler (not the
 * AST itself!).
 *
 * The type table operates in two modes: one is where we just look up the type,
 * check its fields for correctness and mark whether it is polymorphic or not.
 * The second one is where we compute byte sizes, alignment and offsets.
 *
 * The basic algorithm for type resolving and computing byte sizes is to
 * recursively try to lay out each member type of a particular type. This is
 * done in a stack-like fashion, where each embedded type pushes a breadcrumb
 * unto the stack. We may discover a cycle in embedded types (we call this a
 * "type loop"). After which the type table attempts to break the type loop by
 * making specific types heap-allocated. Upon doing so we know their size
 * because their stack-size is now based on pointers. Hence breaking the type
 * loop required for computing the byte size of types.
 *
 * The reason for these type shenanigans is because PDL is a value-based
 * language, but we would still like to be able to express recursively defined
 * types like trees or linked lists. Hence we need to insert pointers somewhere
 * to break these cycles.
 *
 * We will insert these pointers into the variants of unions. However note that
 * we can only compute the stack size of a union until we've looked at *all*
 * variants. Hence we perform an initial pass where we detect type loops, a
 * second pass where we compute the stack sizes of everything, and a third pass
 * where we actually compute the size of the heap allocations for unions.
 *
 * As a final bit of global documentation: non-polymorphic types will always
 * have one "monomorph" entry. This contains the non-polymorphic type's memory
 * layout.
 */

// Programmer note: deduplication of types is currently disabled, see the
// @Deduplication key. Tests might fail when it is re-enabled.
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

use crate::protocol::ast::*;
use crate::protocol::parser::symbol_table::SymbolScope;
use crate::protocol::input_source::ParseError;
use crate::protocol::parser::*;

//------------------------------------------------------------------------------
// Defined Types
//------------------------------------------------------------------------------

/// Struct wrapping around a potentially polymorphic type. If the type does not
/// have any polymorphic arguments then it will not have any monomorphs and
/// `is_polymorph` will be set to `false`. A type with polymorphic arguments
/// only has `is_polymorph` set to `true` if the polymorphic arguments actually
/// appear in the types associated types (function return argument, struct
/// field, enum variant, etc.). Otherwise the polymorphic argument is just a
/// marker and does not influence the bytesize of the type.
#[allow(unused)]
pub struct DefinedType {
    pub(crate) ast_root: RootId,
    pub(crate) ast_definition: DefinitionId,
    pub(crate) definition: DefinedTypeVariant,
    pub(crate) poly_vars: Vec<PolymorphicVariable>,
    pub(crate) is_polymorph: bool,
}

pub enum DefinedTypeVariant {
    Enum(EnumType),
    Union(UnionType),
    Struct(StructType),
    Procedure(ProcedureType),
}

impl DefinedTypeVariant {
    pub(crate) fn is_data_type(&self) -> bool {
        use DefinedTypeVariant as DTV;

        match self {
            DTV::Struct(_) | DTV::Enum(_) | DTV::Union(_) => return true,
            DTV::Procedure(_) => return false,
        }
    }

    pub(crate) fn as_struct(&self) -> &StructType {
        match self {
            DefinedTypeVariant::Struct(v) => v,
            _ => unreachable!()
        }
    }

    pub(crate) fn as_enum(&self) -> &EnumType {
        match self {
            DefinedTypeVariant::Enum(v) => v,
            _ => unreachable!()
        }
    }

    pub(crate) fn as_union(&self) -> &UnionType {
        match self {
            DefinedTypeVariant::Union(v) => v,
            _ => unreachable!()
        }
    }
}

pub struct PolymorphicVariable {
    pub(crate) identifier: Identifier,
    pub(crate) is_in_use: bool, // a polymorphic argument may be defined, but not used by the type definition
}

/// `EnumType` is the classical C/C++ enum type. It has various variants with
/// an assigned integer value. The integer values may be user-defined,
/// compiler-defined, or a mix of the two. If a user assigns the same enum
/// value multiple times, we assume the user is an expert and we consider both
/// variants to be equal to one another.
pub struct EnumType {
    pub variants: Vec<EnumVariant>,
    pub minimum_tag_value: i64,
    pub maximum_tag_value: i64,
    pub tag_type: ConcreteType,
    pub size: usize,
    pub alignment: usize,
}

// TODO: Also support maximum u64 value
pub struct EnumVariant {
    pub identifier: Identifier,
    pub value: i64,
}

/// `UnionType` is the algebraic datatype (or sum type, or discriminated union).
/// A value is an element of the union, identified by its tag, and may contain
/// a single subtype.
/// For potentially infinite types (i.e. a tree, or a linked list) only unions
/// can break the infinite cycle. So when we lay out these unions in memory we
/// will reserve enough space on the stack for all union variants that do not
/// cause "type loops" (i.e. a union `A` with a variant containing a struct
/// `B`). And we will reserve enough space on the heap (and store a pointer in
/// the union) for all variants which do cause type loops (i.e. a union `A`
/// with a variant to a struct `B` that contains the union `A` again).
pub struct UnionType {
    pub variants: Vec<UnionVariant>,
    pub tag_type: ConcreteType,
    pub tag_size: usize,
}

pub struct UnionVariant {
    pub identifier: Identifier,
    pub embedded: Vec<ParserType>, // zero-length does not have embedded values
    pub tag_value: i64,
}

/// `StructType` is a generic C-like struct type (or record type, or product
/// type) type.
pub struct StructType {
    pub fields: Vec<StructField>,
}

pub struct StructField {
    pub identifier: Identifier,
    pub parser_type: ParserType,
}

/// `ProcedureType` is the signature of a procedure/component
pub struct ProcedureType {
    pub kind: ProcedureKind,
    pub return_type: Option<ParserType>,
    pub arguments: Vec<ProcedureArgument>,
}

pub struct ProcedureArgument {
    identifier: Identifier,
    parser_type: ParserType,
}

/// Represents the data associated with a single expression after type inference
/// for a monomorph (or just the normal expression types, if dealing with a
/// non-polymorphic function/component).
pub struct MonomorphExpression {
    // The output type of the expression. Note that for a function it is not the
    // function's signature but its return type
    pub(crate) expr_type: ConcreteType,
    // Has multiple meanings: the field index for select expressions, the
    // monomorph index for polymorphic function calls or literals. Negative
    // values are never used, but used to catch programming errors.
    pub(crate) field_or_monomorph_idx: i32,
    pub(crate) type_id: TypeId,
}

//------------------------------------------------------------------------------
// Type monomorph storage
//------------------------------------------------------------------------------

pub(crate) enum MonoTypeVariant {
    Builtin, // no extra data, added manually in compiler initialization code
    Enum, // no extra data
    Struct(StructMonomorph),
    Union(UnionMonomorph),
    Procedure(ProcedureMonomorph), // functions, components
    Tuple(TupleMonomorph),
}

impl MonoTypeVariant {
    fn as_struct_mut(&mut self) -> &mut StructMonomorph {
        match self {
            MonoTypeVariant::Struct(v) => v,
            _ => unreachable!(),
        }
    }

    pub(crate) fn as_union(&self) -> &UnionMonomorph {
        match self {
            MonoTypeVariant::Union(v) => v,
            _ => unreachable!(),
        }
    }

    fn as_union_mut(&mut self) -> &mut UnionMonomorph {
        match self {
            MonoTypeVariant::Union(v) => v,
            _ => unreachable!(),
        }
    }

    pub(crate) fn as_struct(&self) -> &StructMonomorph {
        match self {
            MonoTypeVariant::Struct(v) => v,
            _ => unreachable!(),
        }
    }

    fn as_tuple_mut(&mut self) -> &mut TupleMonomorph {
        match self {
            MonoTypeVariant::Tuple(v) => v,
            _ => unreachable!(),
        }
    }

    pub(crate) fn as_procedure(&self) -> &ProcedureMonomorph {
        match self {
            MonoTypeVariant::Procedure(v) => v,
            _ => unreachable!(),
        }
    }

    fn as_procedure_mut(&mut self) -> &mut ProcedureMonomorph {
        match self {
            MonoTypeVariant::Procedure(v) => v,
            _ => unreachable!(),
        }
    }
}

/// Struct monomorph
pub struct StructMonomorph {
    pub fields: Vec<StructMonomorphField>,
}

pub struct StructMonomorphField {
    pub type_id: TypeId,
    concrete_type: ConcreteType,
    pub size: usize,
    pub alignment: usize,
    pub offset: usize,
}

/// Union monomorph
pub struct UnionMonomorph {
    pub variants: Vec<UnionMonomorphVariant>,
    pub tag_size: usize, // copied from `UnionType` upon monomorph construction.
    // note that the stack size is in the `TypeMonomorph` struct. This size and
    // alignment will include the size of the union tag.
    //
    // heap_size contains the allocated size of the union in the case it
    // is used to break a type loop. If it is 0, then it doesn't require
    // allocation and lives entirely on the stack.
    pub heap_size: usize,
    pub heap_alignment: usize,
}

pub struct UnionMonomorphVariant {
    pub lives_on_heap: bool,
    pub embedded: Vec<UnionMonomorphEmbedded>,
}

pub struct UnionMonomorphEmbedded {
    pub type_id: TypeId,
    concrete_type: ConcreteType,
    // Note that the meaning of the offset (and alignment) depend on whether or
    // not the variant lives on the stack/heap. If it lives on the stack then
    // they refer to the offset from the start of the union value (so the first
    // embedded type lives at a non-zero offset, because the union tag sits in
    // the front). If it lives on the heap then it refers to the offset from the
    // allocated memory region (so the first embedded type lives at a 0 offset).
    pub size: usize,
    pub alignment: usize,
    pub offset: usize,
}

/// Procedure (functions and components of all possible types) monomorph. Also
/// stores the expression type data from the typechecking/inferencing pass.
pub struct ProcedureMonomorph {
    pub monomorph_index: u32,
    pub builtin: bool,
}

/// Tuple monomorph. Again a kind of exception because one cannot define a named
/// tuple type containing explicit polymorphic variables. But again: we need to
/// store size/offset/alignment information, so we do it here.
pub struct TupleMonomorph {
    pub members: Vec<TupleMonomorphMember>
}

pub struct TupleMonomorphMember {
    pub type_id: TypeId,
    concrete_type: ConcreteType,
    pub size: usize,
    pub alignment: usize,
    pub offset: usize,
}

/// Generic unique type ID. Every monomorphed type and every non-polymorphic
/// type will have one of these associated with it.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct TypeId(i64);

impl TypeId {
    pub(crate) fn new_invalid() -> Self {
        return Self(-1);
    }
}

/// A monomorphed type (or non-polymorphic type's) memory layout and information
/// regarding associated types (like a struct's field type).
pub struct MonoType {
    pub type_id: TypeId,
    pub concrete_type: ConcreteType,
    pub size: usize,
    pub alignment: usize,
    pub(crate) variant: MonoTypeVariant
}

impl MonoType {
    #[inline]
    fn new_empty(type_id: TypeId, concrete_type: ConcreteType, variant: MonoTypeVariant) -> Self {
        return Self {
            type_id, concrete_type,
            size: 0,
            alignment: 0,
            variant,
        }
    }

    /// Little internal helper function as a reminder: if alignment is 0, then
    /// the size/alignment are not actually computed yet!
    #[inline]
    fn get_size_alignment(&self) -> Option<(usize, usize)> {
        if self.alignment == 0 {
            return None
        } else {
            return Some((self.size, self.alignment));
        }
    }
}

/// Special structure that acts like the lookup key for `ConcreteType` instances
/// that have already been added to the type table before.
#[derive(Clone)]
struct MonoSearchKey {
    // Uses bitflags to denote when parts between search keys should match and
    // whether they should be checked. Needs to have a system like this to
    // accommodate tuples.
    parts: Vec<(u8, ConcreteTypePart)>,
    change_bit: u8,
}

impl MonoSearchKey {
    const KEY_IN_USE: u8 = 0x01;
    const KEY_CHANGE_BIT: u8 = 0x02;

    fn with_capacity(capacity: usize) -> Self {
        return MonoSearchKey{
            parts: Vec::with_capacity(capacity),
            change_bit: 0,
        };
    }

    /// Sets the search key based on a single concrete type and its polymorphic
    /// variables.
    fn set(&mut self, concrete_type_parts: &[ConcreteTypePart], poly_var_in_use: &[PolymorphicVariable]) {
        self.set_top_type(concrete_type_parts[0]);

        let mut poly_var_index = 0;
        for subtype in ConcreteTypeIter::new(concrete_type_parts, 0) {
            let in_use = poly_var_in_use[poly_var_index].is_in_use;
            poly_var_index += 1;
            self.push_subtype(subtype, in_use);
        }

        debug_assert_eq!(poly_var_index, poly_var_in_use.len());
    }

    /// Starts setting the search key based on an initial top-level type,
    /// programmer must call `push_subtype` the appropriate number of times
    /// after calling this function
    fn set_top_type(&mut self, type_part: ConcreteTypePart) {
        self.parts.clear();
        self.parts.push((Self::KEY_IN_USE, type_part));
        self.change_bit = Self::KEY_CHANGE_BIT;
    }

    fn push_subtype(&mut self, concrete_type: &[ConcreteTypePart], in_use: bool) {
        let flag = self.change_bit | (if in_use { Self::KEY_IN_USE } else { 0 });

        for part in concrete_type {
            self.parts.push((flag, *part));
        }
        self.change_bit ^= Self::KEY_CHANGE_BIT;
    }

    fn push_subtree(&mut self, concrete_type: &[ConcreteTypePart], poly_var_in_use: &[PolymorphicVariable]) {
        self.parts.push((self.change_bit | Self::KEY_IN_USE, concrete_type[0]));
        self.change_bit ^= Self::KEY_CHANGE_BIT;

        let mut poly_var_index = 0;
        for subtype in ConcreteTypeIter::new(concrete_type, 0) {
            let in_use = poly_var_in_use[poly_var_index].is_in_use;
            poly_var_index += 1;
            self.push_subtype(subtype, in_use);
        }

        debug_assert_eq!(poly_var_index, poly_var_in_use.len());
    }

    // Utilities for hashing and comparison
    fn find_end_index(&self, start_index: usize) -> usize {
        // Check if we're already at the end
        let mut index = start_index;
        if index >= self.parts.len() {
            return index;
        }

        // Iterate until bit flips, or until at end
        let expected_bit = self.parts[index].0 & Self::KEY_CHANGE_BIT;

        index += 1;
        while index < self.parts.len() {
            let current_bit = self.parts[index].0 & Self::KEY_CHANGE_BIT;
            if current_bit != expected_bit {
                return index;
            }

            index += 1;
        }

        return index;
    }
}

impl Hash for MonoSearchKey {
    fn hash<H: Hasher>(&self, state: &mut H) {
        for index in 0..self.parts.len() {
            let (_flags, part) = self.parts[index];
            // if flags & Self::KEY_IN_USE != 0 { @Deduplication
            part.hash(state);
            // }
        }
    }
}

impl PartialEq for MonoSearchKey {
    fn eq(&self, other: &Self) -> bool {
        let mut self_index = 0;
        let mut other_index = 0;

        while self_index < self.parts.len() && other_index < other.parts.len() {
            // Retrieve part and flags
            let (_self_bits, _) = self.parts[self_index];
            let (_other_bits, _) = other.parts[other_index];
            let self_in_use = true; // (self_bits & Self::KEY_IN_USE) != 0; @Deduplication
            let other_in_use = true; // (other_bits & Self::KEY_IN_USE) != 0; @Deduplication

            // Determine ending indices
            let self_end_index = self.find_end_index(self_index);
            let other_end_index = other.find_end_index(other_index);

            if self_in_use == other_in_use {
                if self_in_use {
                    // Both are in use, so both parts should be equal
                    let delta_self = self_end_index - self_index;
                    let delta_other = other_end_index - other_index;
                    if delta_self != delta_other {
                        // Both in use, but not of equal length, so the types
                        // cannot match
                        return false;
                    }

                    for _ in 0..delta_self {
                        let (_, self_part) = self.parts[self_index];
                        let (_, other_part) = other.parts[other_index];

                        if self_part != other_part {
                            return false;
                        }

                        self_index += 1;
                        other_index += 1;
                    }
                } else {
                    // Both not in use, so skip associated parts
                    self_index = self_end_index;
                    other_index = other_end_index;
                }
            } else {
                // No agreement on importance of parts. This is practically
                // impossible
                unreachable!();
            }
        }

        // Everything matched, so if we're at the end of both arrays then we're
        // certain that the two keys are equal.
        return self_index == self.parts.len() && other_index == other.parts.len();
    }
}

impl Eq for MonoSearchKey{}

//------------------------------------------------------------------------------
// Type table
//------------------------------------------------------------------------------

const POLY_VARS_IN_USE: [PolymorphicVariable; 1] = [PolymorphicVariable{ identifier: Identifier::new_empty(InputSpan::new()), is_in_use: true }];

// Programmer note: keep this struct free of dynamically allocated memory
#[derive(Clone)]
struct TypeLoopBreadcrumb {
    type_id: TypeId,
    next_member: u32,
    next_embedded: u32, // for unions, the index into the variant's embedded types
}

// Programmer note: keep this struct free of dynamically allocated memory
#[derive(Clone)]
struct MemoryBreadcrumb {
    type_id: TypeId,
    next_member: u32,
    next_embedded: u32,
    first_size_alignment_idx: u32,
}

#[derive(Debug, PartialEq, Eq)]
enum TypeLoopResult {
    TypeExists,
    PushBreadcrumb(DefinitionId, ConcreteType),
    TypeLoop(usize), // index into vec of breadcrumbs at which the type matched
}

enum MemoryLayoutResult {
    TypeExists(usize, usize), // (size, alignment)
    PushBreadcrumb(MemoryBreadcrumb),
}

// TODO: @Optimize, initial memory-unoptimized implementation
struct TypeLoopEntry {
    type_id: TypeId,
    is_union: bool,
}

struct TypeLoop {
    members: Vec<TypeLoopEntry>,
}

type DefinitionMap = HashMap<DefinitionId, DefinedType>;
type MonoTypeMap = HashMap<MonoSearchKey, TypeId>;
type MonoTypeArray = Vec<MonoType>;

pub struct TypeTable {
    // Lookup from AST DefinitionId to a defined type. Also lookups for
    // concrete type to monomorphs
    pub(crate) definition_lookup: DefinitionMap,
    mono_type_lookup: MonoTypeMap,
    pub(crate) mono_types: MonoTypeArray,
    mono_search_key: MonoSearchKey,
    // Breadcrumbs left behind while trying to find type loops. Also used to
    // determine sizes of types when all type loops are detected.
    type_loop_breadcrumbs: Vec<TypeLoopBreadcrumb>,
    type_loops: Vec<TypeLoop>,
    // Stores all encountered types during type loop detection. Used afterwards
    // to iterate over all types in order to compute size/alignment.
    encountered_types: Vec<TypeLoopEntry>,
    // Breadcrumbs and temporary storage during memory layout computation.
    memory_layout_breadcrumbs: Vec<MemoryBreadcrumb>,
    size_alignment_stack: Vec<(usize, usize)>,
}

impl TypeTable {
    /// Construct a new type table without any resolved types.
    pub(crate) fn new() -> Self {
        Self{ 
            definition_lookup: HashMap::with_capacity(128),
            mono_type_lookup: HashMap::with_capacity(128),
            mono_types: Vec::with_capacity(128),
            mono_search_key: MonoSearchKey::with_capacity(32),
            type_loop_breadcrumbs: Vec::with_capacity(32),
            type_loops: Vec::with_capacity(8),
            encountered_types: Vec::with_capacity(32),
            memory_layout_breadcrumbs: Vec::with_capacity(32),
            size_alignment_stack: Vec::with_capacity(64),
        }
    }

    /// Iterates over all defined types (polymorphic and non-polymorphic) and
    /// add their types in two passes. In the first pass we will just add the
    /// base types (we will not consider monomorphs, and we will not compute
    /// byte sizes). In the second pass we will compute byte sizes of
    /// non-polymorphic types, and potentially the monomorphs that are embedded
    /// in those types.
    pub(crate) fn build_base_types(&mut self, modules: &mut [Module], ctx: &mut PassCtx) -> Result<(), ParseError> {
        // Make sure we're allowed to cast root_id to index into ctx.modules
        debug_assert!(modules.iter().all(|m| m.phase >= ModuleCompilationPhase::DefinitionsParsed));
        debug_assert!(self.definition_lookup.is_empty());

        dbg_code!({
            for (index, module) in modules.iter().enumerate() {
                debug_assert_eq!(index, module.root_id.index as usize);
            }
        });

        // Use context to guess hashmap size of the base types
        let reserve_size = ctx.heap.definitions.len();
        self.definition_lookup.reserve(reserve_size);

        // Resolve all base types
        for definition_idx in 0..ctx.heap.definitions.len() {
            let definition_id = ctx.heap.definitions.get_id(definition_idx);
            let definition = &ctx.heap[definition_id];

            match definition {
                Definition::Enum(_) => self.build_base_enum_definition(modules, ctx, definition_id)?,
                Definition::Union(_) => self.build_base_union_definition(modules, ctx, definition_id)?,
                Definition::Struct(_) => self.build_base_struct_definition(modules, ctx, definition_id)?,
                Definition::Procedure(_) => self.build_base_procedure_definition(modules, ctx, definition_id)?,
            }
        }

        debug_assert_eq!(self.definition_lookup.len(), reserve_size, "mismatch in reserved size of type table");
        for module in modules.iter_mut() {
            module.phase = ModuleCompilationPhase::TypesAddedToTable;
        }

        // Go through all types again, lay out all types that are not
        // polymorphic. This might cause us to lay out monomorphized polymorphs
        // if these were member types of non-polymorphic types.
        for definition_idx in 0..ctx.heap.definitions.len() {
            let definition_id = ctx.heap.definitions.get_id(definition_idx);
            let poly_type = self.definition_lookup.get(&definition_id).unwrap();

            if !poly_type.definition.is_data_type() || !poly_type.poly_vars.is_empty() {
                continue;
            }

            // If here then the type is a data type without polymorphic
            // variables, but we might have instantiated it already, so:
            let concrete_parts = [ConcreteTypePart::Instance(definition_id, 0)];
            self.mono_search_key.set(&concrete_parts, &[]);
            let type_id = self.mono_type_lookup.get(&self.mono_search_key);
            if type_id.is_none() {
                self.detect_and_resolve_type_loops_for(
                    modules, ctx.heap, ctx.arch,
                    ConcreteType{
                        parts: vec![ConcreteTypePart::Instance(definition_id, 0)]
                    },
                )?;
                self.lay_out_memory_for_encountered_types(ctx.arch);
            }
        }

        Ok(())
    }

    /// Retrieves base definition from type table. We must be able to retrieve
    /// it as we resolve all base types upon type table construction (for now).
    /// However, in the future we might do on-demand type resolving, so return
    /// an option anyway
    #[inline]
    pub(crate) fn get_base_definition(&self, definition_id: &DefinitionId) -> Option<&DefinedType> {
        self.definition_lookup.get(&definition_id)
    }

    /// Returns the index into the monomorph type array if the provided type
    /// already has a (reserved) monomorph.
    #[inline]
    pub(crate) fn get_monomorph_type_id(&self, definition_id: &DefinitionId, type_parts: &[ConcreteTypePart]) -> Option<TypeId> {
        // Cannot use internal search key due to mutability issues. But this
        // method should end up being deprecated at some point anyway.
        debug_assert_eq!(get_concrete_type_definition(type_parts).unwrap(), *definition_id);
        let base_type = self.definition_lookup.get(definition_id).unwrap();
        let mut search_key = MonoSearchKey::with_capacity(type_parts.len());
        search_key.set(type_parts, &base_type.poly_vars);

        return self.mono_type_lookup.get(&search_key).copied();
    }

    #[inline]
    pub(crate) fn get_monomorph(&self, type_id: TypeId) -> &MonoType {
        return &self.mono_types[type_id.0 as usize];
    }

    /// Reserves space for a monomorph of a polymorphic procedure. The index
    /// will point into a (reserved) slot of the array of expression types. The
    /// monomorph may NOT exist yet (because the reservation implies that we're
    /// going to be performing typechecking on it, and we don't want to
    /// check the same monomorph twice)
    pub(crate) fn reserve_procedure_monomorph_type_id(&mut self, definition_id: &DefinitionId, concrete_type: ConcreteType, monomorph_index: u32) -> TypeId {
        debug_assert_eq!(get_concrete_type_definition(&concrete_type.parts).unwrap(), *definition_id);
        let type_id = TypeId(self.mono_types.len() as i64);
        let base_type = self.definition_lookup.get_mut(definition_id).unwrap();
        self.mono_search_key.set(&concrete_type.parts, &base_type.poly_vars);

        debug_assert!(!self.mono_type_lookup.contains_key(&self.mono_search_key));
        self.mono_type_lookup.insert(self.mono_search_key.clone(), type_id);
        self.mono_types.push(MonoType::new_empty(type_id, concrete_type, MonoTypeVariant::Procedure(ProcedureMonomorph{
            monomorph_index,
            builtin: false,
        })));

        return type_id;
    }

    /// Adds a builtin type to the type table. As this is only called by the
    /// compiler during setup we assume it cannot fail.
    pub(crate) fn add_builtin_data_type(&mut self, concrete_type: ConcreteType, poly_vars: &[PolymorphicVariable], size: usize, alignment: usize) -> TypeId {
        self.mono_search_key.set(&concrete_type.parts, poly_vars);
        debug_assert!(!self.mono_type_lookup.contains_key(&self.mono_search_key));
        debug_assert_ne!(alignment, 0);
        let type_id = TypeId(self.mono_types.len() as i64);
        self.mono_type_lookup.insert(self.mono_search_key.clone(), type_id);
        self.mono_types.push(MonoType{
            type_id,
            concrete_type,
            size,
            alignment,
            variant: MonoTypeVariant::Builtin,
        });

        return type_id;
    }

    /// Adds a builtin procedure to the type table.
    pub(crate) fn add_builtin_procedure_type(&mut self, concrete_type: ConcreteType, poly_vars: &[PolymorphicVariable]) -> TypeId {
        self.mono_search_key.set(&concrete_type.parts, poly_vars);
        debug_assert!(!self.mono_type_lookup.contains_key(&self.mono_search_key));
        let type_id = TypeId(self.mono_types.len() as i64);
        self.mono_type_lookup.insert(self.mono_search_key.clone(), type_id);
        self.mono_types.push(MonoType{
            type_id,
            concrete_type,
            size: 0,
            alignment: 0,
            variant: MonoTypeVariant::Procedure(ProcedureMonomorph{
                monomorph_index: u32::MAX,
                builtin: true,
            })
        });

        return type_id;
    }

    /// Adds a monomorphed type to the type table. If it already exists then the
    /// previous entry will be used.
    pub(crate) fn add_monomorphed_type(
        &mut self, modules: &[Module], heap: &Heap, arch: &TargetArch, concrete_type: ConcreteType
    ) -> Result<TypeId, ParseError> {
        // Check if the concrete type was already added
        Self::set_search_key_to_type(&mut self.mono_search_key, &self.definition_lookup, &concrete_type.parts);
        if let Some(type_id) = self.mono_type_lookup.get(&self.mono_search_key) {
            return Ok(*type_id);
        }

        // Concrete type needs to be added
        self.detect_and_resolve_type_loops_for(modules, heap, arch, concrete_type)?;
        let type_id = self.encountered_types[0].type_id;
        self.lay_out_memory_for_encountered_types(arch);

        return Ok(type_id);
    }

    //--------------------------------------------------------------------------
    // Building base types
    //--------------------------------------------------------------------------

    /// Builds the base type for an enum. Will not compute byte sizes
    fn build_base_enum_definition(&mut self, modules: &[Module], ctx: &mut PassCtx, definition_id: DefinitionId) -> Result<(), ParseError> {
        debug_assert!(!self.definition_lookup.contains_key(&definition_id), "base enum already built");
        let definition = ctx.heap[definition_id].as_enum();
        let root_id = definition.defined_in;

        // Determine enum variants
        let mut enum_value = -1;
        let mut variants = Vec::with_capacity(definition.variants.len());

        for variant in &definition.variants {
            if enum_value == i64::MAX {
                let source = &modules[definition.defined_in.index as usize].source;
                return Err(ParseError::new_error_str_at_span(
                    source, variant.identifier.span,
                    "this enum variant has an integer value that is too large"
                ));
            }

            enum_value += 1;
            if let EnumVariantValue::Integer(explicit_value) = variant.value {
                enum_value = explicit_value;
            }

            variants.push(EnumVariant{
                identifier: variant.identifier.clone(),
                value: enum_value,
            });
        }

        // Determine tag size
        let mut min_enum_value = 0;
        let mut max_enum_value = 0;
        if !variants.is_empty() {
            min_enum_value = variants[0].value;
            max_enum_value = variants[0].value;
            for variant in variants.iter().skip(1) {
                min_enum_value = min_enum_value.min(variant.value);
                max_enum_value = max_enum_value.max(variant.value);
            }
        }

        let (tag_type, size_and_alignment) = Self::variant_tag_type_from_values(min_enum_value, max_enum_value);

        // Enum names and polymorphic args do not conflict
        Self::check_identifier_collision(
            modules, root_id, &variants, |variant| &variant.identifier, "enum variant"
        )?;

        // Polymorphic arguments cannot appear as embedded types, because
        // they can only consist of integer variants.
        Self::check_poly_args_collision(modules, ctx, root_id, &definition.poly_vars)?;
        let poly_vars = Self::create_polymorphic_variables(&definition.poly_vars);

        self.definition_lookup.insert(definition_id, DefinedType {
            ast_root: root_id,
            ast_definition: definition_id,
            definition: DefinedTypeVariant::Enum(EnumType{
                variants,
                minimum_tag_value: min_enum_value,
                maximum_tag_value: max_enum_value,
                tag_type,
                size: size_and_alignment,
                alignment: size_and_alignment
            }),
            poly_vars,
            is_polymorph: false,
        });

        return Ok(());
    }

    /// Builds the base type for a union. Will compute byte sizes.
    fn build_base_union_definition(&mut self, modules: &[Module], ctx: &mut PassCtx, definition_id: DefinitionId) -> Result<(), ParseError> {
        debug_assert!(!self.definition_lookup.contains_key(&definition_id), "base union already built");
        let definition = ctx.heap[definition_id].as_union();
        let root_id = definition.defined_in;

        // Check all variants and their embedded types
        let mut variants = Vec::with_capacity(definition.variants.len());
        let mut tag_counter = 0;
        for variant in &definition.variants {
            for embedded in &variant.value {
                Self::check_member_parser_type(
                    modules, ctx, root_id, embedded, false
                )?;
            }

            variants.push(UnionVariant{
                identifier: variant.identifier.clone(),
                embedded: variant.value.clone(),
                tag_value: tag_counter,
            });
            tag_counter += 1;
        }

        let mut max_tag_value = 0;
        if tag_counter != 0 {
            max_tag_value = tag_counter - 1
        }

        let (tag_type, tag_size) = Self::variant_tag_type_from_values(0, max_tag_value);

        // Make sure there are no conflicts in identifiers
        Self::check_identifier_collision(
            modules, root_id, &variants, |variant| &variant.identifier, "union variant"
        )?;
        Self::check_poly_args_collision(modules, ctx, root_id, &definition.poly_vars)?;

        // Construct internal representation of union
        let mut poly_vars = Self::create_polymorphic_variables(&definition.poly_vars);
        for variant in &definition.variants {
            for embedded in &variant.value {
                Self::mark_used_polymorphic_variables(&mut poly_vars, embedded);
            }
        }

        let is_polymorph = poly_vars.iter().any(|arg| arg.is_in_use);

        self.definition_lookup.insert(definition_id, DefinedType{
            ast_root: root_id,
            ast_definition: definition_id,
            definition: DefinedTypeVariant::Union(UnionType{ variants, tag_type, tag_size }),
            poly_vars,
            is_polymorph
        });

        return Ok(());
    }

    /// Builds base struct type. Will not compute byte sizes.
    fn build_base_struct_definition(&mut self, modules: &[Module], ctx: &mut PassCtx, definition_id: DefinitionId) -> Result<(), ParseError> {
        debug_assert!(!self.definition_lookup.contains_key(&definition_id), "base struct already built");
        let definition = ctx.heap[definition_id].as_struct();
        let root_id = definition.defined_in;

        // Check all struct fields and construct internal representation
        let mut fields = Vec::with_capacity(definition.fields.len());

        for field in &definition.fields {
            Self::check_member_parser_type(
                modules, ctx, root_id, &field.parser_type, false
            )?;

            fields.push(StructField{
                identifier: field.field.clone(),
                parser_type: field.parser_type.clone(),
            });
        }

        // Make sure there are no conflicting variables
        Self::check_identifier_collision(
            modules, root_id, &fields, |field| &field.identifier, "struct field"
        )?;
        Self::check_poly_args_collision(modules, ctx, root_id, &definition.poly_vars)?;

        // Construct base type in table
        let mut poly_vars = Self::create_polymorphic_variables(&definition.poly_vars);
        for field in &fields {
            Self::mark_used_polymorphic_variables(&mut poly_vars, &field.parser_type);
        }

        let is_polymorph = poly_vars.iter().any(|arg| arg.is_in_use);

        self.definition_lookup.insert(definition_id, DefinedType{
            ast_root: root_id,
            ast_definition: definition_id,
            definition: DefinedTypeVariant::Struct(StructType{ fields }),
            poly_vars,
            is_polymorph
        });

        return Ok(())
    }

    /// Builds base procedure type.
    fn build_base_procedure_definition(&mut self, modules: &[Module], ctx: &mut PassCtx, definition_id: DefinitionId) -> Result<(), ParseError> {
        debug_assert!(!self.definition_lookup.contains_key(&definition_id), "base function already built");
        let definition = ctx.heap[definition_id].as_procedure();
        let root_id = definition.defined_in;

        // Check and construct return types and argument types.
        if let Some(return_type) = &definition.return_type {
            Self::check_member_parser_type(
                modules, ctx, root_id, return_type, definition.source.is_builtin()
            )?;
        }

        let mut arguments = Vec::with_capacity(definition.parameters.len());
        for parameter_id in &definition.parameters {
            let parameter = &ctx.heap[*parameter_id];
            Self::check_member_parser_type(
                modules, ctx, root_id, &parameter.parser_type, definition.source.is_builtin()
            )?;

            arguments.push(ProcedureArgument{
                identifier: parameter.identifier.clone(),
                parser_type: parameter.parser_type.clone(),
            });
        }

        // Check conflict of identifiers
        Self::check_identifier_collision(
            modules, root_id, &arguments, |arg| &arg.identifier, "procedure argument"
        )?;
        Self::check_poly_args_collision(modules, ctx, root_id, &definition.poly_vars)?;

        // Construct internal representation of function type
        // TODO: Marking used polymorphic variables should take statements in
        //  the body into account. But currently we don't. Hence mark them all
        //  as being in-use. Note to self: true condition should be that the
        //  polymorphic variables are used in places where the resulting types
        //  are themselves truly polymorphic types (e.g. not a phantom type).
        let mut poly_vars = Self::create_polymorphic_variables(&definition.poly_vars);
        for poly_var in &mut poly_vars {
            poly_var.is_in_use = true;
        }

        let is_polymorph = poly_vars.iter().any(|arg| arg.is_in_use);

        self.definition_lookup.insert(definition_id, DefinedType{
            ast_root: root_id,
            ast_definition: definition_id,
            definition: DefinedTypeVariant::Procedure(ProcedureType{
                kind: definition.kind,
                return_type: definition.return_type.clone(),
                arguments
            }),
            poly_vars,
            is_polymorph
        });

        return Ok(());
    }

    /// Will check if the member type (field of a struct, embedded type in a
    /// union variant) is valid.
    fn check_member_parser_type(
        modules: &[Module], ctx: &PassCtx, base_definition_root_id: RootId,
        member_parser_type: &ParserType, allow_special_compiler_types: bool
    ) -> Result<(), ParseError> {
        use ParserTypeVariant as PTV;

        for element in &member_parser_type.elements {
            match element.variant {
                // Special cases
                PTV::Void | PTV::InputOrOutput | PTV::ArrayLike | PTV::IntegerLike => {
                    if !allow_special_compiler_types {
                        unreachable!("compiler-only ParserTypeVariant in member type");
                    }
                },
                // Builtin types, always valid
                PTV::Message | PTV::Bool |
                PTV::UInt8 | PTV::UInt16 | PTV::UInt32 | PTV::UInt64 |
                PTV::SInt8 | PTV::SInt16 | PTV::SInt32 | PTV::SInt64 |
                PTV::Character | PTV::String |
                PTV::Array | PTV::Input | PTV::Output | PTV::Tuple(_) |
                // Likewise, polymorphic variables are always valid
                PTV::PolymorphicArgument(_, _) => {},
                // Types that are not constructable, or types that are not
                // allowed (and checked earlier)
                PTV::IntegerLiteral | PTV::Inferred => {
                    unreachable!("illegal ParserTypeVariant within type definition");
                },
                // Finally, user-defined types
                PTV::Definition(definition_id, _) => {
                    let definition = &ctx.heap[definition_id];
                    if !(definition.is_struct() || definition.is_enum() || definition.is_union()) {
                        let source = &modules[base_definition_root_id.index as usize].source;
                        return Err(ParseError::new_error_str_at_span(
                            source, element.element_span, "expected a datatype (a struct, enum or union)"
                        ));
                    }

                    // Otherwise, we're fine
                }
            }
        }

        // If here, then all elements check out
        return Ok(());
    }

    /// Go through a list of identifiers and ensure that all identifiers have
    /// unique names
    fn check_identifier_collision<T: Sized, F: Fn(&T) -> &Identifier>(
        modules: &[Module], root_id: RootId, items: &[T], getter: F, item_name: &'static str
    ) -> Result<(), ParseError> {
        for (item_idx, item) in items.iter().enumerate() {
            let item_ident = getter(item);
            for other_item in &items[0..item_idx] {
                let other_item_ident = getter(other_item);
                if item_ident == other_item_ident {
                    let module_source = &modules[root_id.index as usize].source;
                    return Err(ParseError::new_error_at_span(
                        module_source, item_ident.span, format!("This {} is defined more than once", item_name)
                    ).with_info_at_span(
                        module_source, other_item_ident.span, format!("The other {} is defined here", item_name)
                    ));
                }
            }
        }

        Ok(())
    }

    /// Go through a list of polymorphic arguments and make sure that the
    /// arguments all have unique names, and the arguments do not conflict with
    /// any symbols defined at the module scope.
    fn check_poly_args_collision(
        modules: &[Module], ctx: &PassCtx, root_id: RootId, poly_args: &[Identifier]
    ) -> Result<(), ParseError> {
        // Make sure polymorphic arguments are unique and none of the
        // identifiers conflict with any imported scopes
        for (arg_idx, poly_arg) in poly_args.iter().enumerate() {
            for other_poly_arg in &poly_args[..arg_idx] {
                if poly_arg == other_poly_arg {
                    let module_source = &modules[root_id.index as usize].source;
                    return Err(ParseError::new_error_str_at_span(
                        module_source, poly_arg.span,
                        "This polymorphic argument is defined more than once"
                    ).with_info_str_at_span(
                        module_source, other_poly_arg.span,
                        "It conflicts with this polymorphic argument"
                    ));
                }
            }

            // Check if identifier conflicts with a symbol defined or imported
            // in the current module
            if let Some(symbol) = ctx.symbols.get_symbol_by_name(SymbolScope::Module(root_id), poly_arg.value.as_bytes()) {
                // We have a conflict
                let module_source = &modules[root_id.index as usize].source;
                let introduction_span = symbol.variant.span_of_introduction(ctx.heap);
                return Err(ParseError::new_error_str_at_span(
                    module_source, poly_arg.span,
                    "This polymorphic argument conflicts with another symbol"
                ).with_info_str_at_span(
                    module_source, introduction_span,
                    "It conflicts due to this symbol"
                ));
            }
        }

        // All arguments are fine
        Ok(())
    }

    //--------------------------------------------------------------------------
    // Detecting type loops
    //--------------------------------------------------------------------------

    /// Internal function that will detect type loops and check if they're
    /// resolvable. If so then the appropriate union variants will be marked as
    /// "living on heap". If not then a `ParseError` will be returned
    fn detect_and_resolve_type_loops_for(&mut self, modules: &[Module], heap: &Heap, arch: &TargetArch, concrete_type: ConcreteType) -> Result<(), ParseError> {
        // Programmer notes: what happens here is the we call
        // `check_member_for_type_loops` for a particular type's member, and
        // then take action using the return value:
        // 1. It might already be resolved: in this case it implies we don't
        //  have type loops, or they have been resolved.
        // 2. A new type is encountered. If so then it is added to the type loop
        //  breadcrumbs.
        // 3. A type loop is detected (implying the type is already resolved, or
        //  already exists in the type loop breadcrumbs).
        //
        // Using the breadcrumbs we incrementally check every member type of a
        // particular considered type (e.g. a struct field, tuple member), and
        // do the same as above. Note that when a breadcrumb is added we reserve
        // space in the monomorph storage, initialized to zero-values (i.e.
        // wrong values). The breadcrumbs keep track of how far and along we are
        // with resolving the member types.
        //
        // At the end we may have some type loops. If they're unresolvable then
        // we throw an error). If there are no type loops or they are all
        // resolvable then we end up with a list of `encountered_types`. These
        // are then used by `lay_out_memory_for_encountered_types`.
        debug_assert!(self.type_loop_breadcrumbs.is_empty());
        debug_assert!(self.type_loops.is_empty());
        debug_assert!(self.encountered_types.is_empty());

        // Push the initial breadcrumb
        let initial_breadcrumb = Self::check_member_for_type_loops(
            &self.type_loop_breadcrumbs, &self.definition_lookup, &self.mono_type_lookup,
            &mut self.mono_search_key, &concrete_type
        );

        if let TypeLoopResult::PushBreadcrumb(definition_id, concrete_type) = initial_breadcrumb {
            self.handle_new_breadcrumb_for_type_loops(arch, definition_id, concrete_type);
        } else {
            unreachable!()
        };

        // Enter into the main resolving loop
        while !self.type_loop_breadcrumbs.is_empty() {
            // Because we might be modifying the breadcrumb array we need to
            let breadcrumb_idx = self.type_loop_breadcrumbs.len() - 1;
            let mut breadcrumb = self.type_loop_breadcrumbs[breadcrumb_idx].clone();

            let mono_type = &self.mono_types[breadcrumb.type_id.0 as usize];
            let resolve_result = match &mono_type.variant {
                MonoTypeVariant::Builtin => {
                    TypeLoopResult::TypeExists
                }
                MonoTypeVariant::Enum => {
                    TypeLoopResult::TypeExists
                },
                MonoTypeVariant::Union(monomorph) => {
                    let num_variants = monomorph.variants.len() as u32;
                    let mut union_result = TypeLoopResult::TypeExists;

                    'member_loop: while breadcrumb.next_member < num_variants {
                        let mono_variant = &monomorph.variants[breadcrumb.next_member as usize];
                        let num_embedded = mono_variant.embedded.len() as u32;

                        while breadcrumb.next_embedded < num_embedded {
                            let mono_embedded = &mono_variant.embedded[breadcrumb.next_embedded as usize];
                            union_result = Self::check_member_for_type_loops(
                                &self.type_loop_breadcrumbs, &self.definition_lookup, &self.mono_type_lookup,
                                &mut self.mono_search_key, &mono_embedded.concrete_type
                            );

                            if union_result != TypeLoopResult::TypeExists {
                                // In type loop or new breadcrumb pushed, so
                                // break out of the resolving loop
                                break 'member_loop;
                            }

                            breadcrumb.next_embedded += 1;
                        }

                        breadcrumb.next_embedded = 0;
                        breadcrumb.next_member += 1
                    }

                    union_result
                },
                MonoTypeVariant::Struct(monomorph) => {
                    let num_fields = monomorph.fields.len() as u32;

                    let mut struct_result = TypeLoopResult::TypeExists;
                    while breadcrumb.next_member < num_fields {
                        let mono_field = &monomorph.fields[breadcrumb.next_member as usize];
                        struct_result = Self::check_member_for_type_loops(
                            &self.type_loop_breadcrumbs, &self.definition_lookup, &self.mono_type_lookup,
                            &mut self.mono_search_key, &mono_field.concrete_type
                        );

                        if struct_result != TypeLoopResult::TypeExists {
                            // Type loop or breadcrumb pushed, so break out of
                            // the resolving loop
                            break;
                        }

                        breadcrumb.next_member += 1;
                    }

                    struct_result
                },
                MonoTypeVariant::Procedure(_) => unreachable!(),
                MonoTypeVariant::Tuple(monomorph) => {
                    let num_members = monomorph.members.len() as u32;
                    let mut tuple_result = TypeLoopResult::TypeExists;

                    while breadcrumb.next_member < num_members {
                        let tuple_member = &monomorph.members[breadcrumb.next_member as usize];
                        tuple_result = Self::check_member_for_type_loops(
                            &self.type_loop_breadcrumbs, &self.definition_lookup, &self.mono_type_lookup,
                            &mut self.mono_search_key, &tuple_member.concrete_type
                        );

                        if tuple_result != TypeLoopResult::TypeExists {
                            break;
                        }

                        breadcrumb.next_member += 1;
                    }

                    tuple_result
                }
            };

            // Handle the result of attempting to resolve the current breadcrumb
            match resolve_result {
                TypeLoopResult::TypeExists => {
                    // We finished parsing the type
                    self.type_loop_breadcrumbs.pop();
                },
                TypeLoopResult::PushBreadcrumb(definition_id, concrete_type) => {
                    // We recurse into the member type.
                    self.type_loop_breadcrumbs[breadcrumb_idx] = breadcrumb;
                    self.handle_new_breadcrumb_for_type_loops(arch, definition_id, concrete_type);
                },
                TypeLoopResult::TypeLoop(first_idx) => {
                    // Because we will be modifying breadcrumbs within the
                    // type-loop handling code, put back the modified breadcrumb
                    self.type_loop_breadcrumbs[breadcrumb_idx] = breadcrumb;

                    // We're in a type loop. Add the type loop
                    let mut loop_members = Vec::with_capacity(self.type_loop_breadcrumbs.len() - first_idx);
                    let mut contains_union = false;

                    for breadcrumb_idx in first_idx..self.type_loop_breadcrumbs.len() {
                        let breadcrumb = &mut self.type_loop_breadcrumbs[breadcrumb_idx];
                        let mut is_union = false;

                        // Check if type loop member is a union that may be
                        // broken up by moving some of its members to the heap.
                        let mono_type = &mut self.mono_types[breadcrumb.type_id.0 as usize];
                        if let MonoTypeVariant::Union(union_type) = &mut mono_type.variant {
                            // Mark the variant that caused the loop as heap
                            // allocated to break the type loop.
                            let variant = &mut union_type.variants[breadcrumb.next_member as usize];
                            variant.lives_on_heap = true;
                            breadcrumb.next_embedded += 1;

                            is_union = true;
                            contains_union = true;
                        } // else: we don't care about the type for now

                        loop_members.push(TypeLoopEntry{
                            type_id: breadcrumb.type_id,
                            is_union
                        });
                    }

                    let new_type_loop = TypeLoop{ members: loop_members };
                    if !contains_union {
                        // No way to (potentially) break the union. So return a
                        // type loop error. This is because otherwise our
                        // breadcrumb resolver ends up in an infinite loop.
                        return Err(construct_type_loop_error(
                            &self.mono_types, &new_type_loop, modules, heap
                        ));
                    }

                    self.type_loops.push(new_type_loop);
                }
            }
        }

        // All breadcrumbs have been cleared. So now `type_loops` contains all
        // of the encountered type loops, and `encountered_types` contains a
        // list of all unique monomorphs we encountered.

        // The next step is to figure out if all of the type loops can be
        // broken. A type loop can be broken if at least one union exists in the
        // loop and that union ended up having variants that are not part of
        // a type loop.
        fn type_loop_source_span_and_message<'a>(
            modules: &'a [Module], heap: &Heap, mono_types: &MonoTypeArray,
            definition_id: DefinitionId, mono_type_id: TypeId, index_in_loop: usize
        ) -> (&'a InputSource, InputSpan, String) {
            // Note: because we will discover the type loop the *first* time we
            // instantiate a monomorph with the provided polymorphic arguments
            // (not all arguments are actually used in the type). We don't have
            // to care about a second instantiation where certain unused
            // polymorphic arguments are different.
            let mono_type = &mono_types[mono_type_id.0 as usize];
            let type_name = mono_type.concrete_type.display_name(heap);

            let message = if index_in_loop == 0 {
                format!(
                    "encountered an infinitely large type for '{}' (which can be fixed by \
                    introducing a union type that has a variant whose embedded types are \
                    not part of a type loop, or do not have embedded types)",
                    type_name
                )
            } else if index_in_loop == 1 {
                format!("because it depends on the type '{}'", type_name)
            } else {
                format!("which depends on the type '{}'", type_name)
            };

            let ast_definition = &heap[definition_id];
            let ast_root_id = ast_definition.defined_in();

            return (
                &modules[ast_root_id.index as usize].source,
                ast_definition.identifier().span,
                message
            );
        }

        fn construct_type_loop_error(mono_types: &MonoTypeArray, type_loop: &TypeLoop, modules: &[Module], heap: &Heap) -> ParseError {
            // Seek first entry to produce parse error. Then continue builder
            // pattern. This is the error case so efficiency can go home.
            let mut parse_error = None;
            let mut next_member_index = 0;
            while next_member_index < type_loop.members.len() {
                let first_entry = &type_loop.members[next_member_index];
                next_member_index += 1;

                // Retrieve definition of first type in loop
                let first_mono_type = &mono_types[first_entry.type_id.0 as usize];
                let first_definition_id = get_concrete_type_definition(&first_mono_type.concrete_type.parts);
                if first_definition_id.is_none() {
                    continue;
                }
                let first_definition_id = first_definition_id.unwrap();

                // Produce error message for first type in loop
                let (first_module, first_span, first_message) = type_loop_source_span_and_message(
                    modules, heap, mono_types, first_definition_id, first_entry.type_id, 0
                );
                parse_error = Some(ParseError::new_error_at_span(first_module, first_span, first_message));
                break;
            }

            let mut parse_error = parse_error.unwrap(); // Loop above cannot have failed, because we must have a type loop, type loops cannot contain only unnamed types

            let mut error_counter = 1;
            for member_idx in next_member_index..type_loop.members.len() {
                let entry = &type_loop.members[member_idx];
                let mono_type = &mono_types[entry.type_id.0 as usize];
                let definition_id = get_concrete_type_definition(&mono_type.concrete_type.parts);
                if definition_id.is_none() {
                    continue;
                }
                let definition_id = definition_id.unwrap();

                let (module, span, message) = type_loop_source_span_and_message(
                    modules, heap, mono_types, definition_id, entry.type_id, error_counter
                );
                parse_error = parse_error.with_info_at_span(module, span, message);
                error_counter += 1;
            }

            parse_error
        }

        for type_loop in &self.type_loops {
            let mut can_be_broken = false;
            debug_assert!(!type_loop.members.is_empty());

            for entry in &type_loop.members {
                if entry.is_union {
                    let mono_type = self.mono_types[entry.type_id.0 as usize].variant.as_union();
                    debug_assert!(!mono_type.variants.is_empty()); // otherwise it couldn't be part of the type loop
                    let has_stack_variant = mono_type.variants.iter().any(|variant| !variant.lives_on_heap);
                    if has_stack_variant {
                        can_be_broken = true;
                        break;
                    }
                }
            }

            if !can_be_broken {
                // Construct a type loop error
                return Err(construct_type_loop_error(&self.mono_types, type_loop, modules, heap));
            }
        }

        // If here, then all type loops have been resolved and we can lay out
        // all of the members
        self.type_loops.clear();

        return Ok(());
    }

    /// Checks if the specified type needs to be resolved (i.e. we need to push
    /// a breadcrumb), is already resolved (i.e. we can continue with the next
    /// member of the currently considered type) or is in the process of being
    /// resolved (i.e. we're in a type loop). Because of borrowing rules we
    /// don't do any modifications of internal types here. Hence: if we
    /// return `PushBreadcrumb` then call `handle_new_breadcrumb_for_type_loops`
    /// to take care of storing the appropriate types.
    fn check_member_for_type_loops(
        breadcrumbs: &[TypeLoopBreadcrumb], definition_map: &DefinitionMap, mono_type_map: &MonoTypeMap,
        mono_key: &mut MonoSearchKey, concrete_type: &ConcreteType
    ) -> TypeLoopResult {
        // Depending on the type, lookup if the type has already been visited
        // (i.e. either already has its memory layed out, or is part of a type
        // loop because we've already visited the type)
        debug_assert!(!concrete_type.parts.is_empty());
        let definition_id = if let ConcreteTypePart::Instance(definition_id, _) = concrete_type.parts[0] {
            definition_id
        } else {
            DefinitionId::new_invalid()
        };

        Self::set_search_key_to_type(mono_key, definition_map, &concrete_type.parts);
        if let Some(type_id) = mono_type_map.get(mono_key).copied() {
            for (breadcrumb_idx, breadcrumb) in breadcrumbs.iter().enumerate() {
                if breadcrumb.type_id == type_id {
                    return TypeLoopResult::TypeLoop(breadcrumb_idx);
                }
            }

            return TypeLoopResult::TypeExists;
        }

        // Type is not yet known, so we need to insert it into the lookup and
        // push a new breadcrumb.
        return TypeLoopResult::PushBreadcrumb(definition_id, concrete_type.clone());
    }

    /// Handles the `PushBreadcrumb` result for a `check_member_for_type_loops`
    /// call. Will preallocate entries in the monomorphed type storage (with
    /// all memory properties zeroed).
    fn handle_new_breadcrumb_for_type_loops(&mut self, arch: &TargetArch, definition_id: DefinitionId, concrete_type: ConcreteType) {
        use DefinedTypeVariant as DTV;
        use ConcreteTypePart as CTP;

        let mut is_union = false;

        let type_id = match &concrete_type.parts[0] {
            // Builtin types
            CTP::Void | CTP::Message | CTP::Bool |
            CTP::UInt8 | CTP::UInt16 | CTP::UInt32 | CTP::UInt64 |
            CTP::SInt8 | CTP::SInt16 | CTP::SInt32 | CTP::SInt64 |
            CTP::Character | CTP::String |
            CTP::Array | CTP::Slice | CTP::Input | CTP::Output | CTP::Pointer => {
                // Insert the entry for the builtin type, we should be able to
                // immediately "steal" the size from the preinserted builtins.
                let base_type_id = match &concrete_type.parts[0] {
                    CTP::Void => arch.void_type_id,
                    CTP::Message => arch.message_type_id,
                    CTP::Bool => arch.bool_type_id,
                    CTP::UInt8 => arch.uint8_type_id,
                    CTP::UInt16 => arch.uint16_type_id,
                    CTP::UInt32 => arch.uint32_type_id,
                    CTP::UInt64 => arch.uint64_type_id,
                    CTP::SInt8 => arch.sint8_type_id,
                    CTP::SInt16 => arch.sint16_type_id,
                    CTP::SInt32 => arch.sint32_type_id,
                    CTP::SInt64 => arch.sint64_type_id,
                    CTP::Character => arch.char_type_id,
                    CTP::String => arch.string_type_id,
                    CTP::Array => arch.array_type_id,
                    CTP::Slice => arch.slice_type_id,
                    CTP::Input => arch.input_type_id,
                    CTP::Output => arch.output_type_id,
                    CTP::Pointer => arch.pointer_type_id,
                    _ => unreachable!(),
                };
                let base_type = &self.mono_types[base_type_id.0 as usize];
                let base_type_size = base_type.size;
                let base_type_alignment = base_type.alignment;

                let type_id = TypeId(self.mono_types.len() as i64);
                Self::set_search_key_to_type(&mut self.mono_search_key, &self.definition_lookup, &concrete_type.parts);
                self.mono_type_lookup.insert(self.mono_search_key.clone(), type_id);
                self.mono_types.push(MonoType{
                    type_id,
                    concrete_type,
                    size: base_type_size,
                    alignment: base_type_alignment,
                    variant: MonoTypeVariant::Builtin
                });

                type_id
            },
            // User-defined types
            CTP::Tuple(num_embedded) => {
                debug_assert!(definition_id.is_invalid()); // because tuples do not have an associated `DefinitionId`
                let mut members = Vec::with_capacity(*num_embedded as usize);
                for section in ConcreteTypeIter::new(&concrete_type.parts, 0) {
                    members.push(TupleMonomorphMember{
                        type_id: TypeId::new_invalid(),
                        concrete_type: ConcreteType{ parts: Vec::from(section) },
                        size: 0,
                        alignment: 0,
                        offset: 0
                    });
                }

                let type_id = TypeId(self.mono_types.len() as i64);
                Self::set_search_key_to_tuple(&mut self.mono_search_key, &self.definition_lookup, &concrete_type.parts);
                self.mono_type_lookup.insert(self.mono_search_key.clone(), type_id);
                self.mono_types.push(MonoType::new_empty(type_id, concrete_type, MonoTypeVariant::Tuple(TupleMonomorph{ members })));

                type_id
            },
            CTP::Instance(_check_definition_id, _) => {
                debug_assert_eq!(definition_id, *_check_definition_id); // because this is how `definition_id` was determined

                Self::set_search_key_to_type(&mut self.mono_search_key, &self.definition_lookup, &concrete_type.parts);
                let base_type = self.definition_lookup.get(&definition_id).unwrap();
                let type_id = match &base_type.definition {
                    DTV::Enum(definition) => {
                        // The enum is a bit exceptional in that when we insert
                        // it we we will immediately set its size/alignment:
                        // there is nothing to compute here.
                        debug_assert!(definition.size != 0 && definition.alignment != 0);
                        let type_id = TypeId(self.mono_types.len() as i64);
                        self.mono_type_lookup.insert(self.mono_search_key.clone(), type_id);
                        self.mono_types.push(MonoType::new_empty(type_id, concrete_type, MonoTypeVariant::Enum));

                        let mono_type = &mut self.mono_types[type_id.0 as usize];
                        mono_type.size = definition.size;
                        mono_type.alignment = definition.alignment;

                        type_id
                    },
                    DTV::Union(definition) => {
                        // Create all the variants with their concrete types
                        let mut mono_variants = Vec::with_capacity(definition.variants.len());
                        for poly_variant in &definition.variants {
                            let mut mono_embedded = Vec::with_capacity(poly_variant.embedded.len());
                            for poly_embedded in &poly_variant.embedded {
                                let mono_concrete = Self::construct_concrete_type(poly_embedded, &concrete_type);
                                mono_embedded.push(UnionMonomorphEmbedded{
                                    type_id: TypeId::new_invalid(),
                                    concrete_type: mono_concrete,
                                    size: 0,
                                    alignment: 0,
                                    offset: 0
                                });
                            }

                            mono_variants.push(UnionMonomorphVariant{
                                lives_on_heap: false,
                                embedded: mono_embedded,
                            })
                        }

                        let type_id = TypeId(self.mono_types.len() as i64);
                        let tag_size = definition.tag_size;
                        Self::set_search_key_to_type(&mut self.mono_search_key, &self.definition_lookup, &concrete_type.parts);
                        self.mono_type_lookup.insert(self.mono_search_key.clone(), type_id);
                        self.mono_types.push(MonoType::new_empty(type_id, concrete_type, MonoTypeVariant::Union(UnionMonomorph{
                            variants: mono_variants,
                            tag_size,
                            heap_size: 0,
                            heap_alignment: 0,
                        })));

                        is_union = true;
                        type_id
                    },
                    DTV::Struct(definition) => {
                        // Create fields
                        let mut mono_fields = Vec::with_capacity(definition.fields.len());
                        for poly_field in &definition.fields {
                            let mono_concrete = Self::construct_concrete_type(&poly_field.parser_type, &concrete_type);
                            mono_fields.push(StructMonomorphField{
                                type_id: TypeId::new_invalid(),
                                concrete_type: mono_concrete,
                                size: 0,
                                alignment: 0,
                                offset: 0
                            })
                        }

                        let type_id = TypeId(self.mono_types.len() as i64);
                        Self::set_search_key_to_type(&mut self.mono_search_key, &self.definition_lookup, &concrete_type.parts);
                        self.mono_type_lookup.insert(self.mono_search_key.clone(), type_id);
                        self.mono_types.push(MonoType::new_empty(type_id, concrete_type, MonoTypeVariant::Struct(StructMonomorph{
                            fields: mono_fields,
                        })));

                        type_id
                    },
                    DTV::Procedure(_) => {
                        unreachable!("pushing type resolving breadcrumb for procedure type")
                    },
                };

                type_id
            },
            CTP::Function(_, _) | CTP::Component(_, _) => todo!("function pointers"),
        };

        self.encountered_types.push(TypeLoopEntry{ type_id, is_union });
        self.type_loop_breadcrumbs.push(TypeLoopBreadcrumb{
            type_id,
            next_member: 0,
            next_embedded: 0,
        });
    }

    /// Constructs a concrete type out of a parser type for a struct field or
    /// union embedded type. It will do this by looking up the polymorphic
    /// variables in the supplied concrete type. The assumption is that the
    /// polymorphic variable's indices correspond to the subtrees in the
    /// concrete type.
    fn construct_concrete_type(member_type: &ParserType, container_type: &ConcreteType) -> ConcreteType {
        use ParserTypeVariant as PTV;
        use ConcreteTypePart as CTP;

        // TODO: Combine with code in pass_typing.rs
        fn parser_to_concrete_part(part: &ParserTypeVariant) -> Option<ConcreteTypePart> {
            match part {
                PTV::Void      => Some(CTP::Void),
                PTV::Message   => Some(CTP::Message),
                PTV::Bool      => Some(CTP::Bool),
                PTV::UInt8     => Some(CTP::UInt8),
                PTV::UInt16    => Some(CTP::UInt16),
                PTV::UInt32    => Some(CTP::UInt32),
                PTV::UInt64    => Some(CTP::UInt64),
                PTV::SInt8     => Some(CTP::SInt8),
                PTV::SInt16    => Some(CTP::SInt16),
                PTV::SInt32    => Some(CTP::SInt32),
                PTV::SInt64    => Some(CTP::SInt64),
                PTV::Character => Some(CTP::Character),
                PTV::String    => Some(CTP::String),
                PTV::Array     => Some(CTP::Array),
                PTV::Input     => Some(CTP::Input),
                PTV::Output    => Some(CTP::Output),
                PTV::Tuple(num) => Some(CTP::Tuple(*num)),
                PTV::Definition(definition_id, num) => Some(CTP::Instance(*definition_id, *num)),
                _              => None
            }
        }

        let mut parts = Vec::with_capacity(member_type.elements.len()); // usually a correct estimation, might not be
        for member_part in &member_type.elements {
            // Check if we have a regular builtin type
            if let Some(part) = parser_to_concrete_part(&member_part.variant) {
                parts.push(part);
                continue;
            }

            // Not builtin, but if all code is working correctly, we only care
            // about the polymorphic argument at this point.
            if let PTV::PolymorphicArgument(_container_definition_id, poly_arg_idx) = member_part.variant {
                debug_assert_eq!(_container_definition_id, get_concrete_type_definition(&container_type.parts).unwrap());

                let mut container_iter = container_type.embedded_iter(0);
                for _ in 0..poly_arg_idx {
                    container_iter.next();
                }

                let poly_section = container_iter.next().unwrap();
                parts.extend(poly_section);

                continue;
            }

            unreachable!("unexpected type part {:?} from {:?}", member_part, member_type);
        }

        return ConcreteType{ parts };
    }

    //--------------------------------------------------------------------------
    // Determining memory layout for types
    //--------------------------------------------------------------------------

    /// Should be called after type loops are detected (and resolved
    /// successfully). As a result of this call we expect the
    /// `encountered_types` array to be filled. We'll calculate size/alignment/
    /// offset values for those types in this routine.
    fn lay_out_memory_for_encountered_types(&mut self, arch: &TargetArch) {
        // Programmers note: this works like a little stack machine. We have
        // memory layout breadcrumbs which, like the type loop breadcrumbs, keep
        // track of the currently considered member type. This breadcrumb also
        // stores an index into the `size_alignment_stack`, which will be used
        // to store intermediate size/alignment pairs until all members are
        // resolved. Note that this `size_alignment_stack` is NOT an
        // optimization, we're working around borrowing rules here.

        // Just finished type loop detection, so we're left with the encountered
        // types only. If we don't have any (a builtin type's monomorph was
        // added to the type table) then this function shouldn't be called at
        // all.
        debug_assert!(self.type_loops.is_empty());
        debug_assert!(!self.encountered_types.is_empty());
        debug_assert!(self.memory_layout_breadcrumbs.is_empty());
        debug_assert!(self.size_alignment_stack.is_empty());

        let (ptr_size, ptr_align) = self.mono_types[arch.pointer_type_id.0 as usize].get_size_alignment().unwrap();

        // Push the first entry (the type we originally started with when we
        // were detecting type loops)
        let first_entry = &self.encountered_types[0];
        self.memory_layout_breadcrumbs.push(MemoryBreadcrumb{
            type_id: first_entry.type_id,
            next_member: 0,
            next_embedded: 0,
            first_size_alignment_idx: 0,
        });

        // Enter the main resolving loop
        'breadcrumb_loop: while !self.memory_layout_breadcrumbs.is_empty() {
            let cur_breadcrumb_idx = self.memory_layout_breadcrumbs.len() - 1;
            let mut breadcrumb = self.memory_layout_breadcrumbs[cur_breadcrumb_idx].clone();

            let mono_type = &self.mono_types[breadcrumb.type_id.0 as usize];
            match &mono_type.variant {
                MonoTypeVariant::Builtin | MonoTypeVariant::Enum => {
                    // Size should already be computed
                    dbg_code!({
                        let mono_type = &self.mono_types[breadcrumb.type_id.0 as usize];
                        debug_assert!(mono_type.size != 0 && mono_type.alignment != 0);
                    });
                },
                MonoTypeVariant::Union(mono_type) => {
                    // Retrieve size/alignment of each embedded type. We do not
                    // compute the offsets or total type sizes yet.
                    let num_variants = mono_type.variants.len() as u32;
                    while breadcrumb.next_member < num_variants {
                        let mono_variant = &mono_type.variants[breadcrumb.next_member as usize];

                        if mono_variant.lives_on_heap {
                            // To prevent type loops we made this a heap-
                            // allocated variant. This implies we cannot
                            // compute sizes of members at this point.
                        } else {
                            let num_embedded = mono_variant.embedded.len() as u32;
                            while breadcrumb.next_embedded < num_embedded {
                                let mono_embedded = &mono_variant.embedded[breadcrumb.next_embedded as usize];
                                let layout_result = Self::get_memory_layout_or_breadcrumb(
                                    &self.definition_lookup, &self.mono_type_lookup, &self.mono_types,
                                    &mut self.mono_search_key, arch, &mono_embedded.concrete_type.parts,
                                    self.size_alignment_stack.len()
                                );
                                match layout_result {
                                    MemoryLayoutResult::TypeExists(size, alignment) => {
                                        self.size_alignment_stack.push((size, alignment));
                                    },
                                    MemoryLayoutResult::PushBreadcrumb(new_breadcrumb) => {
                                        self.memory_layout_breadcrumbs[cur_breadcrumb_idx] = breadcrumb;
                                        self.memory_layout_breadcrumbs.push(new_breadcrumb);
                                        continue 'breadcrumb_loop;
                                    }
                                }

                                breadcrumb.next_embedded += 1;
                            }
                        }

                        breadcrumb.next_member += 1;
                        breadcrumb.next_embedded = 0;
                    }

                    // If here then we can at least compute the stack size of
                    // the type, we'll have to come back at the very end to
                    // fill in the heap size/alignment/offset of each heap-
                    // allocated variant.
                    let mut max_size = mono_type.tag_size;
                    let mut max_alignment = mono_type.tag_size;

                    let mono_type = &mut self.mono_types[breadcrumb.type_id.0 as usize];
                    let union_type = mono_type.variant.as_union_mut();
                    let mut size_alignment_idx = breadcrumb.first_size_alignment_idx as usize;

                    for variant in &mut union_type.variants {
                        // We're doing stack computations, so always start with
                        // the tag size/alignment.
                        let mut variant_offset = union_type.tag_size;
                        let mut variant_alignment = union_type.tag_size;

                        if variant.lives_on_heap {
                            // Variant lives on heap, so just a pointer
                            align_offset_to(&mut variant_offset, ptr_align);

                            variant_offset += ptr_size;
                            variant_alignment = variant_alignment.max(ptr_align);
                        } else {
                            // Variant lives on stack, so walk all embedded
                            // types.
                            for embedded in &mut variant.embedded {
                                let (size, alignment) = self.size_alignment_stack[size_alignment_idx];
                                embedded.size = size;
                                embedded.alignment = alignment;
                                size_alignment_idx += 1;

                                align_offset_to(&mut variant_offset, alignment);
                                embedded.offset = variant_offset;

                                variant_offset += size;
                                variant_alignment = variant_alignment.max(alignment);
                            }
                        };

                        max_size = max_size.max(variant_offset);
                        max_alignment = max_alignment.max(variant_alignment);
                    }

                    mono_type.size = max_size;
                    mono_type.alignment = max_alignment;
                    self.size_alignment_stack.truncate(breadcrumb.first_size_alignment_idx as usize);
                },
                MonoTypeVariant::Struct(mono_type) => {
                    // Retrieve size and alignment of each struct member. We'll
                    // compute the offsets once all of those are known
                    let num_fields = mono_type.fields.len() as u32;
                    while breadcrumb.next_member < num_fields {
                        let mono_field = &mono_type.fields[breadcrumb.next_member as usize];

                        let layout_result = Self::get_memory_layout_or_breadcrumb(
                            &self.definition_lookup, &self.mono_type_lookup, &self.mono_types,
                            &mut self.mono_search_key, arch, &mono_field.concrete_type.parts,
                            self.size_alignment_stack.len()
                        );
                        match layout_result {
                            MemoryLayoutResult::TypeExists(size, alignment) => {
                                self.size_alignment_stack.push((size, alignment))
                            },
                            MemoryLayoutResult::PushBreadcrumb(new_breadcrumb) => {
                                self.memory_layout_breadcrumbs[cur_breadcrumb_idx] = breadcrumb;
                                self.memory_layout_breadcrumbs.push(new_breadcrumb);
                                continue 'breadcrumb_loop;
                            },
                        }

                        breadcrumb.next_member += 1;
                    }

                    // Compute offsets and size of total type
                    let mut cur_offset = 0;
                    let mut max_alignment = 1;

                    let mono_type = &mut self.mono_types[breadcrumb.type_id.0 as usize];
                    let struct_type = mono_type.variant.as_struct_mut();
                    let mut size_alignment_idx = breadcrumb.first_size_alignment_idx as usize;

                    for field in &mut struct_type.fields {
                        let (size, alignment) = self.size_alignment_stack[size_alignment_idx];
                        field.size = size;
                        field.alignment = alignment;
                        size_alignment_idx += 1;

                        align_offset_to(&mut cur_offset, alignment);
                        field.offset = cur_offset;

                        cur_offset += size;
                        max_alignment = max_alignment.max(alignment);
                    }

                    mono_type.size = cur_offset;
                    mono_type.alignment = max_alignment;
                    self.size_alignment_stack.truncate(breadcrumb.first_size_alignment_idx as usize);
                },
                MonoTypeVariant::Procedure(_) => {
                    unreachable!();
                },
                MonoTypeVariant::Tuple(mono_type) => {
                    let num_members = mono_type.members.len() as u32;
                    while breadcrumb.next_member < num_members {
                        let mono_member = &mono_type.members[breadcrumb.next_member as usize];
                        let layout_result = Self::get_memory_layout_or_breadcrumb(
                            &self.definition_lookup, &self.mono_type_lookup, &self.mono_types,
                            &mut self.mono_search_key, arch, &mono_member.concrete_type.parts,
                            self.size_alignment_stack.len()
                        );
                        match layout_result {
                            MemoryLayoutResult::TypeExists(size, alignment) => {
                                self.size_alignment_stack.push((size, alignment));
                            },
                            MemoryLayoutResult::PushBreadcrumb(new_breadcrumb) => {
                                self.memory_layout_breadcrumbs[cur_breadcrumb_idx] = breadcrumb;
                                self.memory_layout_breadcrumbs.push(new_breadcrumb);
                                continue 'breadcrumb_loop;
                            },
                        }

                        breadcrumb.next_member += 1;
                    }

                    // If here then we can compute the memory layout of the tuple.
                    let mut cur_offset = 0;
                    let mut max_alignment = 1;

                    let mono_type = &mut self.mono_types[breadcrumb.type_id.0 as usize];
                    let mono_tuple = mono_type.variant.as_tuple_mut();
                    let mut size_alignment_index = breadcrumb.first_size_alignment_idx as usize;
                    for member_index in 0..num_members {
                        let (member_size, member_alignment) = self.size_alignment_stack[size_alignment_index];
                        align_offset_to(&mut cur_offset, member_alignment);
                        size_alignment_index += 1;

                        let member = &mut mono_tuple.members[member_index as usize];
                        member.size = member_size;
                        member.alignment = member_alignment;
                        member.offset = cur_offset;

                        cur_offset += member_size;
                        max_alignment = max_alignment.max(member_alignment);
                    }

                    mono_type.size = cur_offset;
                    mono_type.alignment = max_alignment;
                    self.size_alignment_stack.truncate(breadcrumb.first_size_alignment_idx as usize);
                },
            }

            // If here, then we completely layed out the current type. So move
            // to the next breadcrumb
            self.memory_layout_breadcrumbs.pop();
        }

        debug_assert!(self.size_alignment_stack.is_empty());

        // If here then all types have been layed out. What remains is to
        // compute the sizes/alignment/offsets of the heap variants of the
        // unions we have encountered.
        for entry in &self.encountered_types {
            if !entry.is_union {
                continue;
            }

            // First pass, use buffer to store size/alignment to prevent
            // borrowing issues.
            let mono_type = self.mono_types[entry.type_id.0 as usize].variant.as_union();
            for variant in &mono_type.variants {
                if !variant.lives_on_heap {
                    continue;
                }

                debug_assert!(!variant.embedded.is_empty());

                for embedded in &variant.embedded {
                    let layout_result = Self::get_memory_layout_or_breadcrumb(
                        &self.definition_lookup, &self.mono_type_lookup, &self.mono_types,
                        &mut self.mono_search_key, arch, &embedded.concrete_type.parts,
                        self.size_alignment_stack.len()
                    );
                    match layout_result {
                        MemoryLayoutResult::TypeExists(size, alignment) => {
                            self.size_alignment_stack.push((size, alignment));
                        },
                        _ => unreachable!(), // type was not truly infinite, so type must have been found
                    }
                }
            }

            // Second pass, apply the size/alignment values in our buffer
            let mono_type = self.mono_types[entry.type_id.0 as usize].variant.as_union_mut();

            let mut max_size = 0;
            let mut max_alignment = 1;
            let mut size_alignment_idx = 0;

            for variant in &mut mono_type.variants {
                if !variant.lives_on_heap {
                    continue;
                }

                let mut variant_offset = 0;
                let mut variant_alignment = 1;

                for embedded in &mut variant.embedded {
                    let (size, alignment) = self.size_alignment_stack[size_alignment_idx];
                    embedded.size = size;
                    embedded.alignment = alignment;
                    size_alignment_idx += 1;

                    align_offset_to(&mut variant_offset, alignment);
                    embedded.alignment = variant_offset;

                    variant_offset += size;
                    variant_alignment = variant_alignment.max(alignment);
                }

                max_size = max_size.max(variant_offset);
                max_alignment = max_alignment.max(variant_alignment);
            }

            if max_size != 0 {
                // At least one entry lives on the heap
                mono_type.heap_size = max_size;
                mono_type.heap_alignment = max_alignment;
            }
        }

        // And now, we're actually, properly, done
        self.encountered_types.clear();
        self.size_alignment_stack.clear();
    }

    /// Attempts to compute size/alignment for the provided type. Note that this
    /// is called *after* type loops have been succesfully resolved. Hence we
    /// may assume that all monomorph entries exist, but we may not assume that
    /// those entries already have their size/alignment computed.
    // Passed parameters are messy. But need to strike balance between borrowing
    // and allocations in hot loops. So it is what it is.
    fn get_memory_layout_or_breadcrumb(
        definition_map: &DefinitionMap, mono_type_map: &MonoTypeMap, mono_types: &MonoTypeArray,
        search_key: &mut MonoSearchKey, arch: &TargetArch, parts: &[ConcreteTypePart],
        size_alignment_stack_len: usize,
    ) -> MemoryLayoutResult {
        use ConcreteTypePart as CTP;

        debug_assert!(!parts.is_empty());
        let type_id = match parts[0] {
            CTP::Void      => arch.void_type_id,
            CTP::Message   => arch.message_type_id,
            CTP::Bool      => arch.bool_type_id,
            CTP::UInt8     => arch.uint8_type_id,
            CTP::UInt16    => arch.uint16_type_id,
            CTP::UInt32    => arch.uint32_type_id,
            CTP::UInt64    => arch.uint64_type_id,
            CTP::SInt8     => arch.sint8_type_id,
            CTP::SInt16    => arch.sint16_type_id,
            CTP::SInt32    => arch.sint32_type_id,
            CTP::SInt64    => arch.sint64_type_id,
            CTP::Character => arch.char_type_id,
            CTP::String    => arch.string_type_id,
            CTP::Array     => arch.array_type_id,
            CTP::Slice     => arch.slice_type_id,
            CTP::Input     => arch.input_type_id,
            CTP::Output    => arch.output_type_id,
            CTP::Pointer   => arch.pointer_type_id,
            CTP::Tuple(_) => {
                Self::set_search_key_to_tuple(search_key, definition_map, parts);
                let type_id = mono_type_map.get(&search_key).copied().unwrap();

                type_id
            },
            CTP::Instance(definition_id, _) => {
                // Retrieve entry and the specific monomorph index by applying
                // the full concrete type.
                let definition_type = definition_map.get(&definition_id).unwrap();
                search_key.set(parts, &definition_type.poly_vars);
                let type_id = mono_type_map.get(&search_key).copied().unwrap();

                type_id
            },
            CTP::Function(_, _) | CTP::Component(_, _) => {
                todo!("storage for 'function pointers'");
            }
        };

        let mono_type = &mono_types[type_id.0 as usize];
        if let Some((size, alignment)) = mono_type.get_size_alignment() {
            return MemoryLayoutResult::TypeExists(size, alignment);
        } else {
            return MemoryLayoutResult::PushBreadcrumb(MemoryBreadcrumb{
                type_id,
                next_member: 0,
                next_embedded: 0,
                first_size_alignment_idx: size_alignment_stack_len as u32,
            });
        }
    }

    /// Returns tag concrete type (always a builtin integer type), the size of
    /// that type in bytes (and implicitly, its alignment)
    fn variant_tag_type_from_values(min_val: i64, max_val: i64) -> (ConcreteType, usize) {
        debug_assert!(min_val <= max_val);

        let (part, size) = if min_val >= 0 {
            // Can be an unsigned integer
            if max_val <= (u8::MAX as i64) {
                (ConcreteTypePart::UInt8, 1)
            } else if max_val <= (u16::MAX as i64) {
                (ConcreteTypePart::UInt16, 2)
            } else if max_val <= (u32::MAX as i64) {
                (ConcreteTypePart::UInt32, 4)
            } else {
                (ConcreteTypePart::UInt64, 8)
            }
        } else {
            // Must be a signed integer
            if min_val >= (i8::MIN as i64) && max_val <= (i8::MAX as i64) {
                (ConcreteTypePart::SInt8, 1)
            } else if min_val >= (i16::MIN as i64) && max_val <= (i16::MAX as i64) {
                (ConcreteTypePart::SInt16, 2)
            } else if min_val >= (i32::MIN as i64) && max_val <= (i32::MAX as i64) {
                (ConcreteTypePart::SInt32, 4)
            } else {
                (ConcreteTypePart::SInt64, 8)
            }
        };

        return (ConcreteType{ parts: vec![part] }, size);
    }

    //--------------------------------------------------------------------------
    // Small utilities
    //--------------------------------------------------------------------------

    fn create_polymorphic_variables(variables: &[Identifier]) -> Vec<PolymorphicVariable> {
        let mut result = Vec::with_capacity(variables.len());
        for variable in variables.iter() {
            result.push(PolymorphicVariable{ identifier: variable.clone(), is_in_use: false });
        }

        result
    }

    fn mark_used_polymorphic_variables(poly_vars: &mut Vec<PolymorphicVariable>, parser_type: &ParserType) {
        for element in &parser_type.elements {
            if let ParserTypeVariant::PolymorphicArgument(_, idx) = &element.variant {
                poly_vars[*idx as usize].is_in_use = true;
            }
        }
    }

    /// Sets the search key to a specific type.
    fn set_search_key_to_type(search_key: &mut MonoSearchKey, definition_map: &DefinitionMap, type_parts: &[ConcreteTypePart]) {
        use ConcreteTypePart as CTP;

        match type_parts[0] {
            // Builtin types without any embedded types
            CTP::Void | CTP::Message | CTP::Bool |
            CTP::UInt8 | CTP::UInt16 | CTP::UInt32 | CTP::UInt64 |
            CTP::SInt8 | CTP::SInt16 | CTP::SInt32 | CTP::SInt64 |
            CTP::Character | CTP::String => {
                debug_assert_eq!(type_parts.len(), 1);
                search_key.set_top_type(type_parts[0]);
            },
            // Builtin types with a single nested type
            CTP::Array | CTP::Slice | CTP::Input | CTP::Output | CTP::Pointer => {
                debug_assert_eq!(type_parts[0].num_embedded(), 1);
                search_key.set(type_parts, &POLY_VARS_IN_USE[..1])
            },
            // User-defined types
            CTP::Tuple(_) => {
                Self::set_search_key_to_tuple(search_key, definition_map, type_parts);
            },
            CTP::Instance(definition_id, _) => {
                let definition_type = definition_map.get(&definition_id).unwrap();
                search_key.set(type_parts, &definition_type.poly_vars);
            },
            CTP::Function(_, _) | CTP::Component(_, _) => {
                todo!("implement function pointers")
            },
        }
    }

    fn set_search_key_to_tuple(search_key: &mut MonoSearchKey, definition_map: &DefinitionMap, type_parts: &[ConcreteTypePart]) {
        dbg_code!({
            let is_tuple = if let ConcreteTypePart::Tuple(_) = type_parts[0] { true } else { false };
            assert!(is_tuple);
        });
        search_key.set_top_type(type_parts[0]);
        for subtree in ConcreteTypeIter::new(type_parts, 0) {
            if let Some(definition_id) = get_concrete_type_definition(subtree) {
                // A definition, so retrieve poly var usage info
                let definition_type = definition_map.get(&definition_id).unwrap();
                search_key.push_subtree(subtree, &definition_type.poly_vars);
            } else {
                // Not a definition, so all type information is important
                search_key.push_subtype(subtree, true);
            }
        }
    }
}

#[inline]
fn align_offset_to(offset: &mut usize, alignment: usize) {
    debug_assert!(alignment > 0);
    let alignment_min_1 = alignment - 1;
    *offset += alignment_min_1;
    *offset &= !(alignment_min_1);
}

#[inline]
fn get_concrete_type_definition(concrete_parts: &[ConcreteTypePart]) -> Option<DefinitionId> {
    match concrete_parts[0] {
        ConcreteTypePart::Instance(definition_id, _) => {
            return Some(definition_id)
        },
        ConcreteTypePart::Function(definition_id, _) |
        ConcreteTypePart::Component(definition_id, _) => {
            return Some(definition_id.upcast());
        },
        _ => {
            return None;
        },
    }
}