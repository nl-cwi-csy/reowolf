pub use std::sync::Arc;
pub use std::sync::atomic::{AtomicU64, Ordering};

// Little wrapper for the two atomic ctor/dtor counters
#[derive(Clone)]
pub struct Counters {
    pub ctor: Arc<AtomicU64>,
    pub dtor: Arc<AtomicU64>,
}

impl Counters {
    pub fn new() -> Self {
        return Self{
            ctor: Arc::new(AtomicU64::new(0)),
            dtor: Arc::new(AtomicU64::new(0)),
        };
    }
}

macro_rules! assert_ctor_eq {
        ($counters:expr, $count:expr) => {
            assert_eq!($counters.ctor.load(Ordering::Acquire), $count);
        }
    }

macro_rules! assert_dtor_eq {
        ($counters:expr, $count:expr) => {
            assert_eq!($counters.dtor.load(Ordering::Acquire), $count);
        }
    }

// Utility resource structure that counts the number of constructors and
// destructor calls.
pub struct Resource {
    dtor: Arc<AtomicU64>,
    pub val: u64,
}

impl Resource {
    pub fn new(counters: &Counters, val: u64) -> Self {
        counters.ctor.fetch_add(1, Ordering::SeqCst);
        return Self{ dtor: counters.dtor.clone(), val };
    }
}

impl Drop for Resource {
    fn drop(&mut self) {
        self.dtor.fetch_add(1, Ordering::SeqCst);
    }
}