mod component_pdl;
mod component_context;
mod control_layer;
mod consensus;
mod component;
mod component_random;
mod component_internet;

pub(crate) use component::{Component, CompScheduling};
pub(crate) use component_pdl::{CompPDL};
pub(crate) use component_context::{CompCtx, PortInstruction, PortKind, PortState, PortStateFlag};
pub(crate) use control_layer::{ControlId};

use super::scheduler::*;
use super::runtime::*;

/// If the component is sleeping, then that flag will be atomically set to
/// false. If we're the ones that made that happen then we add it to the work
/// queue.
pub(crate) fn wake_up_if_sleeping(runtime: &RuntimeInner, comp_id: CompId, handle: &CompHandle) {
    use std::sync::atomic::Ordering;

    let should_wake_up = handle.sleeping
        .compare_exchange(true, false, Ordering::AcqRel, Ordering::Acquire)
        .is_ok();

    if should_wake_up {
        let comp_key = unsafe{ comp_id.upgrade() };
        runtime.enqueue_work(comp_key);
    }
}