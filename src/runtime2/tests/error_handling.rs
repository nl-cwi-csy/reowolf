use super::*;

#[test]
fn test_unconnected_component_error() {
    compile_and_create_component("
    comp interact_with_noone() {
        u8[] array = { 5 };
        auto value = array[1];
    }", "interact_with_noone", no_args());
}

#[test]
fn test_connected_uncommunicating_component_error() {
    compile_and_create_component("
    comp crashing_and_burning(out<u32> unused) {
        u8[] array = { 1337 };
        auto value = array[1337];
    }
    comp sitting_idly_waiting(in<u32> never_providing) {
        sync auto a = get(never_providing);
    }
    comp constructor() {
        // Test one way
        // channel a -> b;
        // new sitting_idly_waiting(b);
        // new crashing_and_burning(a);

        // And the other way around
        channel c -> d;
        new crashing_and_burning(c);
        new sitting_idly_waiting(d);
    }", "constructor", no_args())
}

#[test]
fn test_connected_communicating_component_error() {
    compile_and_create_component("
    comp send_and_fail(out<u32> tx) {
        u8[] array = {};
        sync {
            put(tx, 0);
            array[0] = 5;
        }
    }
    comp receive_once(in<u32> rx) {
        sync auto a = get(rx);
    }
    comp constructor() {
        channel a -> b;
        new send_and_fail(a);
        new receive_once(b);

        channel c -> d;
        new receive_once(d);
        new send_and_fail(c);
    }
    ", "constructor", no_args())
}

#[test]
fn test_failing_after_successful_sync() {
    compile_and_create_component("
    comp put_and_fail(out<u8> tx) { sync put(tx, 1); u8 a = {}[0]; }
    comp get_and_fail(in<u8> rx) { sync auto a = get(rx); u8 a = {}[0]; }
    comp put_and_exit(out<u8> tx) { sync put(tx, 2); }
    comp get_and_exit(in<u8> rx) { sync auto a = get(rx); }

    comp constructor() {
        {
            channel a -> b;
            new put_and_fail(a);
            new get_and_exit(b);
        }
        {
            channel a -> b;
            new get_and_exit(b);
            new put_and_fail(a);
        }
        {
            channel a -> b;
            new put_and_exit(a);
            new get_and_fail(b);
        }
        {
            channel a -> b;
            new get_and_fail(b);
            new put_and_exit(a);
        }
    }
    ", "constructor", no_args());
}