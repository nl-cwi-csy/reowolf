use super::*;

#[test]
fn test_transfer_precreated_port_with_owned_peer() {
    compile_and_create_component("
    comp port_sender(out<in<u32>> tx) {
        channel a -> b;
        sync put(tx, b);
    }

    comp port_receiver(in<in<u32>> rx) {
        sync auto a = get(rx);
    }

    comp constructor() {
        channel a -> b;
        new port_sender(a);
        new port_receiver(b);
    }
    ", "constructor", no_args());
}

#[test]
fn test_transfer_precreated_in_struct_with_owned_peer() {
    compile_and_create_component("
    struct PortPair<T> {
        out<T> tx,
        in<T> rx,
    }

    comp port_sender(out<PortPair<u32>> tx) {
        channel created_tx_a -> created_rx_a;
        channel created_tx_b -> created_rx_b;
        sync put(tx, PortPair{ tx: created_tx_a, rx: created_rx_b });
        sync {
            auto val = get(created_rx_a);
            put(created_tx_b, val);
        }
    }

    comp port_receiver(in<PortPair<u32>> rx) {
        channel fake_tx -> fake_rx;
        auto conn = PortPair{ tx: fake_tx, rx: fake_rx };
        sync conn = get(rx);
        sync {
            put(conn.tx, 5);
            auto val = get(conn.rx);
            while (val != 5) {}
        }
    }

    comp constructor() {
        channel tx -> rx;
        new port_sender(tx);
        new port_receiver(rx);
    }
    ", "constructor", no_args());
}

#[test]
fn test_transfer_precreated_port_with_foreign_peer() {
    compile_and_create_component("
    comp port_sender(out<in<u32>> tx, in<u32> to_send) {
        sync put(tx, to_send);
    }

    comp port_receiver(in<in<u32>> rx) {
        sync auto a = get(rx);
    }

    comp constructor() {
        channel tx -> rx;
        channel forgotten -> to_send;
        new port_sender(tx, to_send);
        new port_receiver(rx);
    }
    ", "constructor", no_args());
}

#[test]
fn test_transfer_synccreated_port() {
    compile_and_create_component("
    comp port_sender(out<in<u32>> tx) {
        sync {
            channel a -> b;
            put(tx, b);
        }
    }

    comp port_receiver(in<in<u32>> rx) {
        sync auto a = get(rx);
    }

    comp constructor() {
        channel a -> b;
        new port_sender(a);
        new port_receiver(b);
    }
    ", "constructor", no_args());
}

#[test]
fn test_transfer_precreated_port_with_owned_peer_and_communication() {
    compile_and_create_component("
    comp port_sender(out<in<u32>> tx) {
        channel a -> b;
        sync put(tx, b);
        sync put(a, 1337);
    }

    comp port_receiver(in<in<u32>> rx) {
        channel a -> b; // this is stupid, but we need to have a variable to use
        sync b = get(rx);
        u32 value = 0;
        sync value = get(b);
        while (value != 1337) {}
    }
    comp constructor() {
        channel a -> b;
        new port_sender(a);
        new port_receiver(b);
    }
    ", "constructor", no_args());
}

#[test]
fn test_transfer_precreated_port_with_foreign_peer_and_communication() {
    compile_and_create_component("
    comp port_sender(out<in<u32>> tx, in<u32> to_send) {
        sync put(tx, to_send);
    }

    comp message_transmitter(out<u32> tx) {
        sync put(tx, 1337);
    }

    comp port_receiver(in<in<u32>> rx) {
        channel unused -> b;
        sync b = get(rx);
        u32 value = 0;
        sync value = get(b);
        while (value != 1337) {}
    }

    comp constructor() {
        channel port_tx -> port_rx;
        channel value_tx -> value_rx;
        new port_sender(port_tx, value_rx);
        new port_receiver(port_rx);
        new message_transmitter(value_tx);
    }
    ", "constructor", no_args());
}

#[test]
fn test_transfer_precreated_port_with_owned_peer_back_and_forth() {
    compile_and_create_component("
    comp port_send_and_receive(out<in<u32>> tx, in<in<u32>> rx) {
        channel a -> b;
        sync {
            put(tx, b);
            b = get(rx);
        }
    }

    comp port_receive_and_send(in<in<u32>> rx, out<in<u32>> tx) {
        channel unused -> transferred; // same problem as in different tests
        sync {
            transferred = get(rx);
            put(tx, transferred);
        }
    }

    comp constructor() {
        channel port_tx_forward -> port_rx_forward;
        channel port_tx_backward -> port_rx_backward;

        new port_send_and_receive(port_tx_forward, port_rx_backward);
        new port_receive_and_send(port_rx_forward, port_tx_backward);
    }", "constructor", no_args());
}

#[test]
fn test_transfer_precreated_port_with_foreign_peer_back_and_forth_and_communication() {
    compile_and_create_component("
    comp port_send_and_receive(out<in<u32>> tx, in<in<u32>> rx, in<u32> to_transfer) {
        sync {
            put(tx, to_transfer);
            to_transfer = get(rx);
        }
        sync {
            auto value = get(to_transfer);
            while (value != 1337) {}
        }
    }

    comp port_receive_and_send(in<in<u32>> rx, out<in<u32>> tx) {
        channel unused -> transferred;
        sync {
            transferred = get(rx);
            put(tx, transferred);
        }
    }

    comp value_sender(out<u32> tx) {
        sync put(tx, 1337);
    }

    comp constructor() {
        channel port_tx_forward -> port_rx_forward;
        channel port_tx_backward -> port_rx_backward;
        channel message_tx -> message_rx;
        new port_send_and_receive(port_tx_forward, port_rx_backward, message_rx);
        new port_receive_and_send(port_rx_forward, port_tx_backward);
        new value_sender(message_tx);
    }
    ", "constructor", no_args());
}