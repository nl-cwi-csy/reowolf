use super::*;


#[test]
fn test_component_communication() {
    let pd = ProtocolDescription::parse(b"
    comp sender(out<u32> o, u32 outside_loops, u32 inside_loops) {
        u32 outside_index = 0;
        while (outside_index < outside_loops) {
            u32 inside_index = 0;
            sync while (inside_index < inside_loops) {
                put(o, inside_index);
                inside_index += 1;
            }
            outside_index += 1;
        }
    }

    comp receiver(in<u32> i, u32 outside_loops, u32 inside_loops) {
        u32 outside_index = 0;
        while (outside_index < outside_loops) {
            u32 inside_index = 0;
            sync while (inside_index < inside_loops) {
                auto val = get(i);
                while (val != inside_index) {} // infinite loop if incorrect value is received
                inside_index += 1;
            }
            outside_index += 1;
        }
    }

    comp constructor() {
        channel o_orom -> i_orom;
        channel o_mrom -> i_mrom;
        channel o_ormm -> i_ormm;
        channel o_mrmm -> i_mrmm;

        // one round, one message per round
        new sender(o_orom, 1, 1);
        new receiver(i_orom, 1, 1);

        // multiple rounds, one message per round
        new sender(o_mrom, 5, 1);
        new receiver(i_mrom, 5, 1);

        // one round, multiple messages per round
        new sender(o_ormm, 1, 5);
        new receiver(i_ormm, 1, 5);

        // multiple rounds, multiple messages per round
        new sender(o_mrmm, 5, 5);
        new receiver(i_mrmm, 5, 5);
    }").expect("compilation");
    let rt = Runtime::new(3, LOG_LEVEL, pd).unwrap();
    create_component(&rt, "", "constructor", no_args());
}

#[test]
fn test_send_to_self() {
    compile_and_create_component("
    comp insane_in_the_membrane() {
        channel a -> b;
        sync {
            put(a, 1);
            auto v = get(b);
            while (v != 1) {}
        }
    }
    ", "insane_in_the_membrane", no_args());
}

#[test]
fn test_intermediate_messenger() {
    let pd = ProtocolDescription::parse(b"
    comp receiver<T>(in<T> rx, u32 num) {
        auto index = 0;
        while (index < num) {
            sync { auto v = get(rx); }
            index += 1;
        }
    }

    comp middleman<T>(in<T> rx, out<T> tx, u32 num) {
        auto index = 0;
        while (index < num) {
            sync { put(tx, get(rx)); }
            index += 1;
        }
    }

    comp sender<T>(out<T> tx, u32 num) {
        auto index = 0;
        while (index < num) {
            sync put(tx, 1337);
            index += 1;
        }
    }

    comp constructor_template<T>() {
        auto num = 0;
        channel<T> tx_a -> rx_a;
        channel tx_b -> rx_b;
        new sender(tx_a, 3);
        new middleman(rx_a, tx_b, 3);
        new receiver(rx_b, 3);
    }

    comp constructor() {
        new constructor_template<u16>();
        new constructor_template<u32>();
        new constructor_template<u64>();
        new constructor_template<s16>();
        new constructor_template<s32>();
        new constructor_template<s64>();
    }
    ").expect("compilation");
    let rt = Runtime::new(3, LOG_LEVEL, pd).unwrap();
    create_component(&rt, "", "constructor", no_args());
}
