// Utility for performing debug printing within a particular module. Still
// requires some extra macros to be defined to be ergonomic.
macro_rules! enabled_debug_print {
    (false, $name:literal, $format:literal) => {};
    (false, $name:literal, $format:literal, $($args:expr),*) => {};
    (true, $name:literal, $format:literal) => {
        println!("[{}] {}", $name, $format)
    };
    (true, $name:literal, $format:literal, $($args:expr),*) => {
        println!("[{}] {}", $name, format!($format, $($args),*))
    };
}

// Utility for inserting code only executed in debug mode. Because writing the
// conditional cfg is tedious and looks ugly. Still doesn't work for struct
// fields, though.
macro_rules! dbg_code {
    ($code:stmt) => {
        #[cfg(debug_assertions)] $code
    }
}

// Given a function name, return type and variant, will generate the all-so
// common `union_value.as_variant()` method. The return value is the reference
// to the embedded union type.
macro_rules! union_cast_to_ref_method_impl {
    ($func_name:ident, $ret_type:ty, $variant:path) => {
        fn $func_name(&self) -> &$ret_type {
            match self {
                $variant(content) => return content,
                _ => unreachable!(),
            }
        }
    }
}

// Another union cast, but now returning a copy of the value
macro_rules! union_cast_to_value_method_impl {
    ($func_name:ident, $ret_type:ty, $variant:path) => {
        impl Value {
            pub(crate) fn $func_name(&self) -> $ret_type {
                match self {
                    $variant(v) => *v,
                    _ => unreachable!(),
                }
            }
        }
    }
}