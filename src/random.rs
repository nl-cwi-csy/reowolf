/**
 * random.rs
 *
 * Simple wrapper over a random number generator. Put here so that we can have
 * a feature flag for particular forms of randomness. For now we'll use pseudo-
 * randomness since that will help debugging.
 */

use rand::{RngCore, SeedableRng};
use rand_pcg;

pub(crate) struct Random {
    rng: rand_pcg::Lcg64Xsh32,
}

impl Random {
    pub(crate) fn new() -> Self {
        use std::time::SystemTime;

        let now = SystemTime::now();
        let elapsed = match now.duration_since(SystemTime::UNIX_EPOCH) {
            Ok(elapsed) => elapsed,
            Err(err) => err.duration(),
        };

        let elapsed = elapsed.as_nanos();
        let seed = elapsed.to_le_bytes();

        return Self::new_seeded(seed);
    }

    pub(crate) fn new_seeded(seed: [u8; 16]) -> Self {
        return Self{ rng: rand_pcg::Pcg32::from_seed(seed) }
    }

    pub(crate) fn get_u64(&mut self) -> u64 {
        return self.rng.next_u64();
    }
}