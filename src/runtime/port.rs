use super::ConnectorId;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct PortIdLocal {
    pub index: u32,
}

impl PortIdLocal {
    pub fn new(id: u32) -> Self {
        Self{ index: id }
    }

    // TODO: Unsure about this, maybe remove, then also remove all struct
    //  instances where I call this
    pub fn new_invalid() -> Self {
        Self{ index: u32::MAX }
    }

    pub fn is_valid(&self) -> bool {
        return self.index != u32::MAX;
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct ChannelId {
    pub index: u32,
}

impl ChannelId {
    pub fn new(id: u32) -> Self {
        return Self{ index: id };
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum PortKind {
    Putter,
    Getter,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum PortState {
    Open,
    Closed,
}

/// Represents a port inside of the runtime. This is generally the local view of
/// a connector on its port, which may not be consistent with the rest of the
/// global system (e.g. its peer was moved to a new connector, or the peer might
/// have died in the meantime, so it is no longer usable).
#[derive(Clone)]
pub struct Port {
    pub self_id: PortIdLocal,
    pub peer_id: PortIdLocal,
    pub channel_id: ChannelId,
    pub kind: PortKind,
    pub state: PortState,
    pub peer_connector: ConnectorId, // might be temporarily inconsistent while peer port is sent around in non-sync phase
}

pub struct Channel {
    pub putter_id: PortIdLocal, // can put on it, so from the connector's point of view, this is an output
    pub getter_id: PortIdLocal, // vice versa: can get on it, so an input for the connector
}