// basics.rs
//
// The most basic of testing: sending a message, receiving a message, etc.

use super::*;

#[test]
fn test_doing_nothing() {
    // If this thing does not get into an infinite loop, (hence: the runtime
    // exits), then the test works
    const CODE: &'static str ="
    comp silent_willy(u32 loops) {
        u32 index = 0;
        while (index < loops) {
            sync { index += 1; }
        }
    }
    ";

    let _timer = TestTimer::new("doing_nothing");
    run_test_in_runtime(CODE, |api| {
        api.create_connector("", "silent_willy", ValueGroup::new_stack(vec![
            Value::UInt32(NUM_LOOPS),
        ])).expect("create component");
    });
}

#[test]
fn test_single_put_and_get() {
    const CODE: &'static str = "
    comp putter(out<bool> sender, u32 loops) {
        u32 index = 0;
        while (index < loops) {
            sync {
                put(sender, true);
            }
            index += 1;
        }
    }

    comp getter(in<bool> receiver, u32 loops) {
        u32 index = 0;
        while (index < loops) {
            sync {
                auto result = get(receiver);
                assert(result);
            }
            index += 1;
        }
    }
    ";

    let _timer = TestTimer::new("single_put_and_get");
    run_test_in_runtime(CODE, |api| {
        let channel = api.create_channel().unwrap();

        api.create_connector("", "putter", ValueGroup::new_stack(vec![
            Value::Output(PortId::new(channel.putter_id.index)),
            Value::UInt32(NUM_LOOPS)
        ])).expect("create putter");

        api.create_connector("", "getter", ValueGroup::new_stack(vec![
            Value::Input(PortId::new(channel.getter_id.index)),
            Value::UInt32(NUM_LOOPS)
        ])).expect("create getter");
    });
}

#[test]
fn test_combined_put_and_get() {
    const CODE: &'static str = "
    comp put_then_get(out<bool> output, in<bool> input, u32 num_loops) {
        u32 index = 0;
        while (index < num_loops) {
            sync {
                put(output, true);
                auto value = get(input);
                assert(value);
                index += 1;
            }
        }
    }

    comp constructor(u32 num_loops) {
        channel output_a -> input_a;
        channel output_b -> input_b;
        new put_then_get(output_a, input_b, num_loops);
        new put_then_get(output_b, input_a, num_loops);
    }
    ";

    run_test_in_runtime(CODE, |api| {
        api.create_connector("", "constructor", ValueGroup::new_stack(vec![
            Value::UInt32(NUM_LOOPS),
        ])).expect("create connector");
    })
}

#[test]
fn test_multi_put_and_get() {
    const CODE: &'static str = "
    comp putter_static(out<u8> vals, u32 num_loops) {
        u32 index = 0;
        while (index < num_loops) {
            sync {
                put(vals, 0b00000001);
                put(vals, 0b00000100);
                put(vals, 0b00010000);
                put(vals, 0b01000000);
            }
            index += 1;
        }
    }

    comp getter_dynamic(in<u8> vals, u32 num_loops) {
        u32 loop_index = 0;
        while (loop_index < num_loops) {
            sync {
                u32 recv_index = 0;
                u8 expected = 1;
                while (recv_index < 4) {
                    auto gotten = get(vals);
                    assert(gotten == expected);
                    expected <<= 2;
                    recv_index += 1;
                }
            }
            loop_index += 1;
        }
    }
    ";

    let _timer = TestTimer::new("multi_put_and_get");
    run_test_in_runtime(CODE, |api| {
        let channel = api.create_channel().unwrap();
        api.create_connector("", "putter_static", ValueGroup::new_stack(vec![
            Value::Output(PortId::new(channel.putter_id.index)),
            Value::UInt32(NUM_LOOPS),
        ])).unwrap();
        api.create_connector("", "getter_dynamic", ValueGroup::new_stack(vec![
            Value::Input(PortId::new(channel.getter_id.index)),
            Value::UInt32(NUM_LOOPS),
        ])).unwrap();
    })
}