mod network_shapes;
mod api_component;
mod speculation;
mod data_transmission;
mod sync_failure;

use super::*;
use crate::ProtocolDescription;
use crate::protocol::eval::*;
use crate::runtime::native::{ApplicationSyncAction};

// Generic testing constants, use when appropriate to simplify stress-testing
// pub(crate) const NUM_THREADS: u32 =  8;     // number of threads in runtime
// pub(crate) const NUM_INSTANCES: u32 = 750;  // number of test instances constructed
// pub(crate) const NUM_LOOPS: u32 = 10;       // number of loops within a single test (not used by all tests)

pub(crate) const NUM_THREADS: u32 = 6;
pub(crate) const NUM_INSTANCES: u32 = 2;
pub(crate) const NUM_LOOPS: u32 = 1;


fn create_runtime(pdl: &str) -> Runtime {
    let protocol = ProtocolDescription::parse(pdl.as_bytes()).expect("parse pdl");
    let runtime = Runtime::new(NUM_THREADS, protocol);

    return runtime;
}

fn run_test_in_runtime<F: Fn(&mut ApplicationInterface)>(pdl: &str, constructor: F) {
    let protocol = ProtocolDescription::parse(pdl.as_bytes())
        .expect("parse PDL");
    let runtime = Runtime::new(NUM_THREADS, protocol);

    let mut api = runtime.create_interface();
    for _ in 0..NUM_INSTANCES {
        constructor(&mut api);
    }
}

pub(crate) struct TestTimer {
    name: &'static str,
    started: std::time::Instant
}

impl TestTimer {
    pub(crate) fn new(name: &'static str) -> Self {
        Self{ name, started: std::time::Instant::now() }
    }
}

impl Drop for TestTimer {
    fn drop(&mut self) {
        let delta = std::time::Instant::now() - self.started;
        let nanos = (delta.as_secs_f64() * 1_000_000.0) as u64;
        let millis = nanos / 1000;
        let nanos = nanos % 1000;
        println!("[{}] Took {:>4}.{:03} ms", self.name, millis, nanos);
    }
}
