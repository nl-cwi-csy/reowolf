// Testing speculation - Basic forms

use super::*;

#[test]
fn test_maybe_do_nothing() {
    // Three variants in which the behaviour in which nothing is performed is
    // somehow not allowed. Note that we "check" by seeing if the test finishes.
    // Only the branches in which ports fire increment the loop index
    const CODE: &'static str = "
    comp only_puts(out<bool> output, u32 num_loops) {
        u32 index = 0;
        while (index < num_loops) {
            sync { put(output, true); }
            index += 1;
        }
    }

    comp might_put(out<bool> output, u32 num_loops) {
        u32 index = 0;
        while (index < num_loops) {
            sync {
                fork { put(output, true); index += 1; }
                or   {}
            }
        }
    }

    comp only_gets(in<bool> input, u32 num_loops) {
        u32 index = 0;
        while (index < num_loops) {
            sync { auto res = get(input); assert(res); }
            index += 1;
        }
    }

    comp might_get(in<bool> input, u32 num_loops) {
        u32 index = 0;
        while (index < num_loops) {
            sync fork { auto res = get(input); assert(res); index += 1; } or {}
        }
    }
    ";

    // Construct all variants which should work and wait until the runtime exits
    run_test_in_runtime(CODE, |api| {
        // only putting -> maybe getting
        let channel = api.create_channel().unwrap();
        api.create_connector("", "only_puts", ValueGroup::new_stack(vec![
            Value::Output(PortId::new(channel.putter_id.index)),
            Value::UInt32(NUM_LOOPS),
        ])).unwrap();
        api.create_connector("", "might_get", ValueGroup::new_stack(vec![
            Value::Input(PortId::new(channel.getter_id.index)),
            Value::UInt32(NUM_LOOPS),
        ])).unwrap();

        // maybe putting -> only getting
        let channel = api.create_channel().unwrap();
        api.create_connector("", "might_put", ValueGroup::new_stack(vec![
            Value::Output(PortId::new(channel.putter_id.index)),
            Value::UInt32(NUM_LOOPS),
        ])).unwrap();
        api.create_connector("", "only_gets", ValueGroup::new_stack(vec![
            Value::Input(PortId::new(channel.getter_id.index)),
            Value::UInt32(NUM_LOOPS),
        ])).unwrap();
    })
}